<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MaterializeAsset extends AssetBundle
{
    public $sourcePath = "@bower/materialize";
    public $css = [
        'dist/css/materialize.min.css'
    ];
    public $js = [
        'dist/js/materialize.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
