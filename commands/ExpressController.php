<?php

namespace app\commands;

use app\models\Express;
use app\models\ExpressPart;
use app\models\League;
use app\models\Match;
use yii\console\Controller;

class ExpressController extends Controller
{

    /**
     * Создает экспрессы для каждой лиги
     *
     */
    public function actionMake(){

        $mLeagues = League::find()->all();
        foreach($mLeagues as $mLeague){

            //делаем прогнозы на сегодня и на завтра
            $unixArray = [
                strtotime(date("d.m.Y")),
                strtotime("+1 day", strtotime(date("d.m.Y")))
            ];

            foreach($unixArray as $unixToday){

                //смотрим сегодняшнюю дату
                //$unixToday = strtotime(date("d.m.Y")); //26.09.15 => 1443225600
                $unixTomorrow = strtotime("+1 day", $unixToday);
                //echo date("d.m.Y H:i:s");
                //var_dump($unixToday);
                //var_dump($unixTomorrow);

                //если для этой лиги нет экспресса на эту дату, то создаем
                $mExpress = Express::findOne(["league_id" => $mLeague->id, "unix_date" => $unixToday]);
                if(empty($mExpress)){
                    $mExpress = new Express();
                    $mExpress->unix_date = $unixToday;
                    $mExpress->league_id = $mLeague->id;
                    $mExpress->save();
                }

                //находим все матчи этой лиги за сегодня
                $mMatches = Match::find()->where("league_id = :league_id AND (predict_goals_home + predict_goals_away >= 2) AND unix_date BETWEEN :unix_today and :unix_tomorrow",[
                    ':league_id' => $mLeague->id,
                    ':unix_today' => $unixToday,
                    ':unix_tomorrow' => $unixTomorrow
                ])->asArray()->all();

                $matches = Express::SortByGoalsProb($mMatches);
                $matches = Express::getMatchesByCoef($matches);

                //если для экспресса на эту дату еще нет матчей или есть матчи
                if(empty($mExpress->expressParts) || !empty($matches)){

                    foreach($mExpress->expressParts as $eP){
                        ExpressPart::deleteAll(['id' => $eP->id]);
                    }

                    foreach($matches as $match){
                        $mExpressPart = new ExpressPart();
                        $mExpressPart->match_id = $match['id'];
                        $mExpressPart->express_id = $mExpress->id;
                        $mExpressPart->coef_param_id = $match['coef_param_id'];
                        $mExpressPart->predict_goals_home = $match['predict_goals_home'];
                        $mExpressPart->predict_goals_away = $match['predict_goals_away'];
                        $mExpressPart->save();
                    }

                }

            }

        }

    }

    /**
     * Создает "мировой" экспресс из всех матчей
     */
    public function actionMakeWorld(){

        //делаем прогнозы на сегодня и на завтра
        $unixArray = [
            strtotime(date("d.m.Y")),
            strtotime("+1 day", strtotime(date("d.m.Y")))
        ];

        foreach($unixArray as $unixToday){

            //смотрим сегодняшнюю дату
            //$unixToday = strtotime(date("d.m.Y")); //26.09.15 => 1443225600
            $unixTomorrow = strtotime("+1 day", $unixToday);

            //если для этой лиги нет экспресса на эту дату, то создаем
            $mExpress = Express::findOne(["league_id" => Express::WORLD_LEAGUE_ID, "unix_date" => $unixToday]);
            if(empty($mExpress)){
                $mExpress = new Express();
                $mExpress->unix_date = $unixToday;
                $mExpress->league_id = Express::WORLD_LEAGUE_ID;
                $mExpress->save();
            }

            //находим все матчи за сегодня
            $mMatches = Match::find()->where("(predict_goals_home + predict_goals_away >= 2) AND unix_date BETWEEN :unix_today and :unix_tomorrow",[
                ':unix_today' => $unixToday,
                ':unix_tomorrow' => $unixTomorrow
            ])->asArray()->all();

            $matches = Express::SortByGoalsProb($mMatches);
            $matches = Express::getMatchesByCoef($matches);

            //если для экспресса на эту дату еще нет матчей или есть матчи
            if(empty($mExpress->expressParts) || !empty($matches)){

                foreach($mExpress->expressParts as $eP){
                    ExpressPart::deleteAll(['id' => $eP->id]);
                }

                foreach($matches as $match){
                    $mExpressPart = new ExpressPart();
                    $mExpressPart->match_id = $match['id'];
                    $mExpressPart->express_id = $mExpress->id;
                    $mExpressPart->coef_param_id = $match['coef_param_id'];
                    $mExpressPart->predict_goals_home = $match['predict_goals_home'];
                    $mExpressPart->predict_goals_away = $match['predict_goals_away'];
                    $mExpressPart->save();
                }

            }

        }

    }

}