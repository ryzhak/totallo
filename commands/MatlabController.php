<?php

namespace app\commands;

use app\models\League;
use app\models\Match;
use app\models\Stat;
use app\models\Team;
use yii\console\Controller;

class MatlabController extends Controller
{

    /**
     * Создает dat файлы для matlab для определенной лиги или для всех
     *
     * @param integer $leagueId
     */
    public function actionCreateDatFiles($leagueId = 0){
        //echo \Yii::getAlias("@app"); return;
        if($leagueId == 0){
            $mLeagues = League::find()->all();
        } else {
            $mLeagues = League::find()->where(['id' => $leagueId])->all();
        }

        if(empty($mLeagues)){
            echo "League(s) not found\n";
            return;
        }

        //перебираем все лиги
        foreach($mLeagues as $mLeague){

            //перебираем все команды лиги
            $mTeams = Team::findAll(['league_id' => $mLeague->id]);
            foreach($mTeams as $mTeam){

                //находим все матчи дома
                $mHomeMatches = Match::find()->where("home_team_id = :home_team_id AND (home_team_goals != '-' OR away_team_goals != '-')", [
                    ':home_team_id' => $mTeam->id
                ])->all();

                //путь для dat файлов
                $path = \Yii::getAlias("@app") . "/matlab_files/dat_files/" .
                    strtolower($mTeam->league->country->eng_name) . "/" .
                    strtolower(str_replace(" ", "", $mTeam->league->eng_name)) . "/";
                //если нет такого пути, то создаем новый
                if(!file_exists($path)){
                    mkdir($path, 0775, true);
                }
                //путь до fis файлов
                $pathFis = \Yii::getAlias("@app") . "/matlab_files/fis_files/" .
                    strtolower($mTeam->league->country->eng_name) . "/" .
                    strtolower(str_replace(" ", "", $mTeam->league->eng_name)) . "/";
                //если нет, то создаем
                if(!file_exists($pathFis)){
                    mkdir($pathFis, 0775, true);
                }
                $pathAttackHome = $path . str_replace(" ","",$mTeam->eng_name) . "_ATTACK_HOME.dat";
                $pathDefendHome = $path . str_replace(" ","",$mTeam->eng_name) . "_DEFEND_HOME.dat";
                $dataAttackHome = "";
                $dataDefendHome = "";
                //перебираем все домашние матчи
                foreach($mHomeMatches as $mHomeMatch){

                    //статистика гостевой команды

                    //строим атакующую модель дома
                    //пропущенные мячи в гостях среднее
                    $p1 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 7])->away_avg;
                    //удары по воротам(соперник) среднее
                    $p2 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 18])->away_avg;
                    //удары в створ(соперник) среднее
                    $p3 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 20])->away_avg;
                    //реализация ударов(соперник) среднее
                    $p4 = trim(str_replace("%","",Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 24])->away_sum));
                    //угловые(соперник) среднее
                    $p5 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 28])->away_avg;
                    $dataAttackHome .= "$p1 $p2 $p3 $p4 $p5 {$mHomeMatch->home_team_goals}\r\n";

                    //строим защитную модель дома
                    //забитые мячи в гостях среднее
                    $p1 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 6])->away_avg;
                    //удары по воротам в гостях среднее
                    $p2 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 17])->away_avg;
                    //удары в створ в гостях среднее
                    $p3 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 19])->away_avg;
                    //реализация ударов в гостях среднее
                    $p4 = trim(str_replace("%","",Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 23])->away_sum));
                    //угловые в гостях среднее
                    $p5 = Stat::findOne(['team_id' => $mHomeMatch->away_team_id, 'stat_param_id' => 27])->away_avg;
                    $dataDefendHome .= "$p1 $p2 $p3 $p4 $p5 {$mHomeMatch->away_team_goals}\r\n";

                }

                //записываем файл
                file_put_contents($pathAttackHome, $dataAttackHome);
                file_put_contents($pathDefendHome, $dataDefendHome);

            }

        }

    }

}