<?php

namespace app\commands;

use app\components\ryzhak\championatparser\ChampionatParser;
use app\components\ryzhak\marathonparser\MarathonParser;
use app\models\Coef;
use app\models\League;
use app\models\Match;
use app\models\Stat;
use app\models\StatParam;
use app\models\Team;
use yii\console\Controller;

class ParseController extends Controller
{
    /**
     * Парсим календарь по id лиги, или весь календарь, если leagueId = 0
     * Время: 7 минут
     *
     * @param int $leagueId
     * @return string
     */
    public function actionChampionatCalendar($leagueId = 0){

        if($leagueId == 0){
            $mLeagues = League::find()->all();
        } else {
            $mLeagues = League::find()->where(['id' => $leagueId])->all();
        }

        if(empty($mLeagues)){
            echo "League(s) not found\n";
            return;
        }

        foreach($mLeagues as $mLeague){

            //получаем все матчи
            $matches = ChampionatParser::getCalendar($mLeague->championat_table_url);
            foreach($matches as $match){

                //если в БД нет такой домашней команды, то вставляем новую
                if(!$mHomeTeam = Team::findOne(['league_id' => $mLeague->id, 'name' => $match['homeTeam']])){
                    $mHomeTeam = new Team();
                    $mHomeTeam->league_id = $mLeague->id;
                    $mHomeTeam->name = $match['homeTeam'];
                    $mHomeTeam->save();
                } else {
                    $mHomeTeam = Team::findOne(['league_id' => $mLeague->id, 'name' => $match['homeTeam']]);
                }

                //если в БД нет такой гостевой команды, то вставляем новую
                if(!$mAwayTeam = Team::findOne(['league_id' => $mLeague->id, 'name' => $match['awayTeam']])){
                    $mAwayTeam = new Team();
                    $mAwayTeam->league_id = $mLeague->id;
                    $mAwayTeam->name = $match['awayTeam'];
                    $mAwayTeam->save();
                } else {
                    $mAwayTeam = Team::findOne(['league_id' => $mLeague->id, 'name' => $match['awayTeam']]);
                }

                //если нет такой игры, то добавляем новую
                if(!$mMatch = Match::findOne([
                    'league_id' => $mLeague->id,
                    'tour_number' => $match['tourNumber'],
                    'home_team_id' => $mHomeTeam->id,
                    'away_team_id' => $mAwayTeam->id
                ])){
                    $mMatch = new Match();
                    $mMatch->league_id = $mLeague->id;
                    $mMatch->tour_number = $match['tourNumber'];
                    $mMatch->home_team_id = $mHomeTeam->id;
                    $mMatch->away_team_id = $mAwayTeam->id;
                    $mMatch->home_team_goals = $match['homeTeamGoals'];
                    $mMatch->away_team_goals = $match['awayTeamGoals'];
                    $mMatch->unix_date = $match['unixDate'];
                    $mMatch->save();
                } else {
                    //иначе обновляем счет и дату игры
                    $mMatch = Match::findOne([
                        'league_id' => $mLeague->id,
                        'tour_number' => $match['tourNumber'],
                        'home_team_id' => $mHomeTeam->id,
                        'away_team_id' => $mAwayTeam->id
                    ]);
                    $mMatch->predict_goals_home = (string)$mMatch->predict_goals_home;
                    $mMatch->predict_goals_away = (string)$mMatch->predict_goals_away;
                    $mMatch->home_team_goals = $match['homeTeamGoals'];
                    $mMatch->away_team_goals = $match['awayTeamGoals'];
                    $mMatch->unix_date = (int)$match['unixDate'];
                    if(!$mMatch->save()){
                        var_dump($mMatch->errors);
                        echo "Error saving match";
                    }
                }

            }
        }

    }

    /**
     * Парсит статистику определенной лиги или всех лиг
     * Время: 20 минут
     *
     * @param $leagueId
     */
    public function actionStats($leagueId = 0){

        if($leagueId == 0){
            $mLeagues = League::find()->all();
        } else {
            $mLeagues = League::find()->where(['id' => $leagueId])->all();
        }

        if(empty($mLeagues)){
            echo "League(s) not found\n";
            return;
        }

        //для всех лиг
        foreach($mLeagues as $mLeague){

            //получаем статистику по всем командам
            $teamStats = ChampionatParser::getStats($mLeague->championat_table_url);
            foreach($teamStats as $teamName => $stats){

                //находим модель команды
                $mTeam = Team::findOne(['name' => $teamName, 'league_id' => $mLeague->id]);

                //перебираем все параметры статистики для одной команды
                foreach($stats as $stat){

                    //если в БД нет такого параметра статистики, то создаем новый
                    if(!$mStatParam = StatParam::findOne(['name' => $stat['name']])){
                        $mStatParam = new StatParam();
                        $mStatParam->name = $stat['name'];
                        $mStatParam->save();
                    } else {
                        $mStatParam = StatParam::findOne(['name' => $stat['name']]);
                    }

                    //если в БД нет статистики для данной команды
                    if(!$mStat = Stat::findOne(['stat_param_id' => $mStatParam->id,'team_id' => $mTeam->id])){
                        $mStat = new Stat();
                        $mStat->stat_param_id = $mStatParam->id;
                        $mStat->team_id = $mTeam->id;
                    } else {
                        $mStat = Stat::findOne(['stat_param_id' => $mStatParam->id,'team_id' => $mTeam->id]);
                    }
                    $mStat->all_sum = $stat["all"]["sum"];
                    $mStat->all_avg = $stat["all"]["avg"];
                    $mStat->home_sum = $stat["home"]["sum"];
                    $mStat->home_avg = $stat["home"]["avg"];
                    $mStat->away_sum = $stat["away"]["sum"];
                    $mStat->away_avg = $stat["away"]["avg"];
                    $mStat->save();

                }

            }

        }
    }

    /**
     * Парсит коэффициенты с сайта betmarathon.com для одной лиги или для всех
     * Время:
     *
     * @param $leagueId
     */
    public function actionMarathon($leagueId = 0){

        if($leagueId == 0){
            $mLeagues = League::find()->all();
        } else {
            $mLeagues = League::find()->where(['id' => $leagueId])->all();
        }

        if(empty($mLeagues)){
            echo "League(s) not found\n";
            return;
        }

        //перебираем все лиги
        foreach($mLeagues as $mLeague){

            //перебираем все матчи с коэффициентами для одной лиги
            $matches = MarathonParser::getCoefs($mLeague->marathon_url);

            foreach($matches as $match){
                
                //модель домашней команды
                $mHomeTeam = Team::findOne(['eng_name' => trim($match['homeTeam'])]);
                //модель гостевой команды
                $mAwayTeam = Team::findOne(['eng_name' => trim($match['awayTeam'])]);

                //если в БД есть такая игра
                if($mMatch = Match::findOne([
                    'home_team_id' => $mHomeTeam->id,
                    'away_team_id' => $mAwayTeam->id,
                    'unix_date' => $match['unixDate']
                ])){
                    //если коэффициенты на игру есть, то обновляем
                    if($mCoefs = Coef::findAll(['match_id' => $mMatch->id])){

                        foreach($mCoefs as $mCoef){
                            if($mCoef->coef_param_id == Coef::TOTAL_OVER_15){
                                $mCoef->value = $match['TO15'];
                                $mCoef->save();
                            }
                            if($mCoef->coef_param_id == Coef::TOTAL_OVER_2){
                                $mCoef->value = $match['TO2'];
                                $mCoef->save();
                            }
                        }

                    } else {

                        //иначе создаем новые
                        $mCoefTO15 = new Coef();
                        $mCoefTO15->match_id = $mMatch->id;
                        $mCoefTO15->coef_param_id = Coef::TOTAL_OVER_15;
                        $mCoefTO15->value = $match['TO15'];
                        if(!$mCoefTO15->save()){
                            var_dump($mCoefTO15->errors);
                            echo "Error saving coefs TO15";
                        }
                        //$mCoefTO15->save();
                        $mCoefTO2 = new Coef();
                        $mCoefTO2->match_id = $mMatch->id;
                        $mCoefTO2->coef_param_id = Coef::TOTAL_OVER_2;
                        $mCoefTO2->value = $match['TO2'];
                        if(!$mCoefTO2->save()){
                            var_dump($mCoefTO2->errors);
                            echo "Error saving coefs TO2";
                        }
                        //$mCoefTO2->save();
                    }
                }

            }

        }

    }

}
