<?php

namespace app\components\ryzhak\championatparser;

use SimpleHtmlDom as SHD;

class ChampionatParser
{

    /**
     * Возвращает массив с календарем всех игр для чемпионата
     *
     * @param $fullTableUrl url основной таблицы чемпионата
     * @return array
     */
    public static function getCalendar($fullTableUrl){

        $calendarUrl = str_replace("table/all", "calendar", $fullTableUrl);

        $calendarHtml = SHD\file_get_html($calendarUrl);
        //перебираем все строки с играми
        $index = 0;
        $trs = $calendarHtml->find("table.table tbody tr");
        $trsCount = count($trs);
        $gameIndex = 0;
        $calendar = [];
        foreach($trs as $tr){

            $index++;

            //если первая строка, на которой заголовки таблиц
            if($index == 1) continue;
            //если последняя строка из футера
            if($index == $trsCount) continue;

            //ищем номер тура
            $tourNumber = $tr->find("td.sport__calendar__table__tour", 0)->plaintext;
            //ищем дату игры
            $dateDirty = trim($tr->find("td.sport__calendar__table__date", 0)->plaintext);
            $unixDate = strtotime(str_replace(",","",$dateDirty));
            //ищем домашнюю команду
            $homeTeam = $tr->find("td.sport__calendar__table__teams a", 0)->plaintext;
            //ищем гостевую команду
            $awayTeam = $tr->find("td.sport__calendar__table__teams a", 1)->plaintext;
            //голы домашней команды
            $homeTeamGoals = trim($tr->find("span.sport__calendar__table__result__left", 0)->plaintext);
            //голы гостевой команды
            $awayTeamGoals = trim($tr->find("span.sport__calendar__table__result__right", 0)->plaintext);

            $calendar[$gameIndex]["tourNumber"] = $tourNumber;
            $calendar[$gameIndex]["unixDate"] = $unixDate;
            $calendar[$gameIndex]["homeTeam"] = $homeTeam;
            $calendar[$gameIndex]["awayTeam"] = $awayTeam;
            $calendar[$gameIndex]["homeTeamGoals"] = $homeTeamGoals;
            $calendar[$gameIndex]["awayTeamGoals"] = $awayTeamGoals;

            $gameIndex++;
        }

        return $calendar;
    }

    /**
     * По url полной таблицы возвращает статистику всех команд
     *
     * @param $fullTableUrl
     * @return array
     */
    public static function getStats($fullTableUrl){

        $fullTableHtml = SHD\file_get_html($fullTableUrl);

        //перебираем все команды в таблице
        $index = 0;
        $params = [];

        foreach($fullTableHtml->find("table.table tbody tr") as $tr){

            $index++;
            //пропускаем строки с названием чемпионата и заголовком таблиц
            if($index <= 2) continue;

            //если не можем найти название команды, например в чемпионате сербии есть 4 лишних строки
            if(!($tr->find("a", 0))) continue;
            //находим название команды
            $teamName = $tr->find("a", 0)->plaintext;

            //находим url со статистикой команды
            $resultHref = $tr->find("a", 0)->href;
            $statsUrl = "http://www.championat.com" . str_replace("result.html", "tstat.html", $resultHref);

            //получаем статистику для команды
            $params[$teamName] = self::getStatsByUrl($statsUrl);

        }

        return $params;

    }

    /**
     * По url страницы возвращает все доступные параметры статистики для команды
     *
     * @param $statsUrl url со статистикой команды
     * @return array
     */
    public static function getStatsByUrl($statsUrl){

        $statsHtml = SHD\file_get_html($statsUrl);

        //находим таблицу статистики
        $statsTable = $statsHtml->find("table.table", 0);
        //перебираем все строки со статистикой
        $index = 0;
        $paramIndex = 0;
        $params = [];
        foreach($statsTable->find("tbody tr") as $tr){

            $index++;
            //если 2 строки с заголовками таблиц, то пропускаем их
            if($index <= 2) continue;

            //находим название параметра статистики
            $params[$paramIndex]["name"] = $tr->find("td", 0)->plaintext;
            //обнуляем все параметры
            $params[$paramIndex]["all"] = ["sum" => "","avg" => ""];
            $params[$paramIndex]["home"] = ["sum" => "","avg" => ""];
            $params[$paramIndex]["away"] = ["sum" => "","avg" => ""];

            //если строка с 3 колонками, а не с 6
            if($tr->find("td[class=_center _group_start _group_end]")){
                //всего
                $params[$paramIndex]["all"]["sum"] = $tr->find("td", 1)->plaintext;
                //дома
                $params[$paramIndex]["home"]["sum"] = $tr->find("td", 2)->plaintext;
                //на выезде
                $params[$paramIndex]["away"]["sum"] = $tr->find("td", 3)->plaintext;
            } else {
                //всего сумма
                $params[$paramIndex]["all"]["sum"] = $tr->find("td", 1)->plaintext;
                //всего среднее
                $params[$paramIndex]["all"]["avg"] = $tr->find("td", 2)->plaintext;
                //дома сумма
                $params[$paramIndex]["home"]["sum"] = $tr->find("td", 3)->plaintext;
                //дома среднее
                $params[$paramIndex]["home"]["avg"] = $tr->find("td", 4)->plaintext;
                //в гостях сумма
                $params[$paramIndex]["away"]["sum"] = $tr->find("td", 5)->plaintext;
                //в гостях среднее
                $params[$paramIndex]["away"]["avg"] = $tr->find("td", 6)->plaintext;
            }

            $paramIndex++;
        }

        return $params;
    }

}