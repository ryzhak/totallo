<?php

namespace app\components\ryzhak\marathonparser;

use SimpleHtmlDom as SHD;

class MarathonParser
{

    const MARATHON_ADDITIONAL_COEFS_URL = "https://www.betmarathon.com/en/markets.htm";

    /**
     * По url страницы с коэффициентами для чемпионата возвращает игру и коэффициенты
     *
     * @param $url url с коэффициентами на все матчи чемпионата
     * @return array
     */
    public static function getCoefs($url){

        $pageHtml = SHD\file_get_html($url);

        //находим все доступные игры
        $games = [];
        $index = 0;

        /**
         * бывает, что страницы с коэффициентами еще нет и марафон выводит вместо нее
         * страницу с матчами всех лиг. На странице со всеми матчами заголовки в h2.category-header,
         * а на странице с отдельной лигой - с нужным нам h1.category-header
         */
        if(!$pageHtml->find("h1.category-header", 0)) return $games;

        foreach($pageHtml->find("tr.event-header") as $tr){

            //хозяева
            if($tr->find("div.today-member-name", 0)){
                $homeTeam = $tr->find("div.today-member-name", 0)->plaintext;
            } else {
                $homeTeam = $tr->find("div.member-name", 0)->plaintext;
            }
            //гости
            if($tr->find("div.today-member-name", 1)){
                $awayTeam = $tr->find("div.today-member-name", 1)->plaintext;
            } else {
                $awayTeam = $tr->find("div.member-name", 1)->plaintext;
            }

            //дата
            $dateDirty = $tr->find("td.date", 0)->plaintext;
            //если дата в формате "16:30"
            if(strlen($dateDirty) == 14){
                $unixDate = strtotime(trim($dateDirty));
            } elseif(strlen($dateDirty) == 21) {
                //если дата в формате "18 Sep 16:00", то добавляем год
                $unixDate = strtotime(self::addYear($dateDirty));
            }
            //добавляем 2 час к дате для UTC+3
            $unixDate = strtotime("+3 hours", $unixDate);

            //находим treeid для получения всех клэффициентов
            $treeId = $tr->find("a.event-more-view", 0)->treeid;

            $coefs = self::getCoeffsByTreeId($treeId);

            $games[$index] = [
                "homeTeam" => $homeTeam,
                "awayTeam" => $awayTeam,
                "unixDate" => $unixDate,
                "treeId" => $treeId,
                "TO15" => $coefs['to15'],
                "TO2" => $coefs['to2']
            ];

            $index++;
        }

        return $games;
    }

    /**
     * По treeid возвращает нужные коэффициенты
     *
     * @param $treeId
     * @return array
     */
    public static function getCoeffsByTreeId($treeId){

        // This is the data to POST to the form. The KEY of the array is the name of the field. The value is the value posted.
        $data_to_post = [];
        $data_to_post['treeId'] = $treeId;

        // Initialize cURL
        $curl = curl_init();
        // Set the options
        curl_setopt($curl,CURLOPT_URL, self::MARATHON_ADDITIONAL_COEFS_URL);
        // This sets the number of fields to post
        curl_setopt($curl,CURLOPT_POST, sizeof($data_to_post));
        //return data instead of just show ot
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // This is the fields to post in the form of an array.
        curl_setopt($curl,CURLOPT_POSTFIELDS, $data_to_post);
        //execute the post
        $result = curl_exec($curl);
        //close the connection
        curl_close($curl);
        $output = json_decode($result, true);
        //echo $output["ADDITIONAL_MARKETS"];

        $html = SHD\str_get_html($output["ADDITIONAL_MARKETS"]);
        $coefs = [
            "to15" => "",
            "to2" => ""
        ];
        //для каждого блока коэффициентов
        foreach($html->find("div.block-market-wrapper") as $div){
            $marketName = trim($div->find("div.market-table-name", 0)->plaintext);
            //если это тоталы
            if($marketName == "Total Goals"){
                $totalGoalsDiv = $div->find("div.market-inline-block-table-wrapper", 0);
                //находим все строки тоталов
                foreach($totalGoalsDiv->find("tr") as $tr){
                    //находим ТБ
                    if($tr->find("td.price", 1)){
                        $tdWithCoef = $tr->find("td.price", 1);
                        //если есть название коэффициента
                        if($tdWithCoef->find("div.coeff-handicap", 0)){
                            $divCoefName = trim($tdWithCoef->find("div.coeff-handicap", 0)->plaintext);
                            if($divCoefName == "(1.5)"){
                                $coefs["to15"] = self::toEuropeCoef($tdWithCoef->find("div.coeff-price", 0)->plaintext);
                            }
                            if($divCoefName == "(2.0)"){
                                $coefs["to2"] = self::toEuropeCoef($tdWithCoef->find("div.coeff-price", 0)->plaintext);
                            }
                        }
                    }
                }
            }
        }

        return $coefs;
    }

    /**
     * Добавляем год в дату на marathon
     *
     * @param $date
     * @return mixed|string
     * @throws \Exception
     * @return string
     */
    public static function addYear($date){

        $res = "";
        if(strpos($date, "Jan")){$res = str_replace("Jan","Jan " . date('Y'), $date);}
        if(strpos($date, "Feb")){$res = str_replace("Feb","Feb " . date('Y'), $date);}
        if(strpos($date, "Mar")){$res = str_replace("Mar","Mar " . date('Y'), $date);}
        if(strpos($date, "Apr")){$res = str_replace("Apr","Apr " . date('Y'), $date);}
        if(strpos($date, "May")){$res = str_replace("May","May " . date('Y'), $date);}
        if(strpos($date, "Jun")){$res = str_replace("Jun","Jun " . date('Y'), $date);}
        if(strpos($date, "Jul")){$res = str_replace("Jul","Jul " . date('Y'), $date);}
        if(strpos($date, "Aug")){$res = str_replace("Aug","Aug " . date('Y'), $date);}
        if(strpos($date, "Sep")){$res = str_replace("Sep","Sep " . date('Y'), $date);}
        if(strpos($date, "Oct")){$res = str_replace("Oct","Oct " . date('Y'), $date);}
        if(strpos($date, "Nov")){$res = str_replace("Nov","Nov " . date('Y'), $date);}
        if(strpos($date, "Dec")){$res = str_replace("Dec","Dec " . date('Y'), $date);}

        if(empty($res)){
            throw new \Exception("Can not add year to date");
        }

        return $res;
    }

    /**
     * Конвертирует коэффициент в европейское представление, из 21/100 в 1.21
     *
     * @param $coef
     * @return float
     */
    public static function toEuropeCoef($coef){
        $numbers = explode("/", $coef);
        return round($numbers[0] / $numbers[1], 2) + 1;
    }

}