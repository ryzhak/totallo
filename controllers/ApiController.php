<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Express;

class ApiController extends Controller
{

    /**
     * Возвращает последние $count экспрессов для всех лиг
     *
     * @param $count
     * @return mixed
     */
    public function actionExpresses($count = 0){
        $resp = Express::getLast($count);
        \Yii::$app->response->format = 'json';
        return $resp;
    }

}