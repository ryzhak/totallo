<?php

namespace app\controllers;

use app\models\Express;
use app\models\Match;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\components\ryzhak\championatparser\ChampionatParser;
use app\components\ryzhak\marathonparser\MarathonParser;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Отображает все экспрессы для определенной лиги
     *
     * @param int $league_id
     * @return string
     */
    public function actionAccumulators($league_id = 0){
        if(empty($league_id)){
            $mExpresses = Express::find()->where(['league_id' => Express::WORLD_LEAGUE_ID])->orderBy('unix_date DESC')->all();
        } else {
            $mExpresses = Express::find()->where(['league_id' => $league_id])->orderBy('unix_date DESC')->all();
        }
        return $this->render("accumulators",['mExpresses' => $mExpresses, 'leagueID' => $league_id]);
    }

    public function actionTestParser(){
        /*$ms = Match::find()->all();
        foreach($ms as $m){
            $m->predict_goals_home = rand(0,6);
            $m->predict_goals_away = rand(0,6);
            $m->save(false);;
        }*/

        //$c = ChampionatParser::getCalendar("http://www.championat.com/football/_russia1d/1335/table/all.html");
        //var_dump($c);
        //$s = ChampionatParser::getStats("http://www.championat.com/football/_england/1597/table/all/group.html");
        //var_dump($s);
        //$games = MarathonParser::getCoefs("https://www.betmarathon.com/en/betting/Football/Russia/FNL");
        //var_dump($games);
    }

}
