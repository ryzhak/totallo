%добавляем postgresql driver
javaaddpath(strcat(pwd,'/../postgresql-9.4-1202.jdbc4.jar'))

%подключаемся к БД
datasourcename = 'totallo'; %Name of database
driver ='org.postgresql.Driver'; %Driver for postgres in MATLAB
databaseurl = 'jdbc:postgresql://localhost:5433/totallo'; %URL for postgres jdbc in MATLAB
password = '123574896'; %Set password
conn = database(datasourcename,'postgres',password,driver,databaseurl);

%выбираем все лиги
sql = exec(conn,strcat('SELECT * FROM league'));
sql = fetch(sql);
leagues = sql.data;
numLeagues = length(leagues);
%перебираем все лиги и генерируем fis модели для каждой команды
for i = 1:numLeagues
    
    %тест для РФПЛ
    %if leagues{i,1} == 1 %|| leagues{i,1} == 3 || leagues{i,1} == 5 || leagues{i,1} == 7 || leagues{i,1} == 9
        
        %название страны на английском маленькими буквами
        sql2 = exec(conn, strcat('SELECT eng_name FROM country WHERE id =', num2str(leagues{i,4})));
        sql2 = fetch(sql2);
        country = sql2.data;
        country = lower(country{1,1});
        %название лиги на английском маленькими буквами
        league = strrep(lower(leagues{i,3}), ' ', '');
        
        %для каждого dat файла делаем fis модель
        pathDatFiles = strcat(pwd,'/../dat_files/',country,'/',league);
        datfiles = dir(strcat(pathDatFiles,'/*.dat'));
        countFiles = length(datfiles);
        for j = 1 : countFiles
            
            disp(datfiles(j).name)
            trainData = load(strcat(pathDatFiles,'/',datfiles(j).name));
            numMFs = 3;
            mfType = 'trimf';
            epoch = 10;
            inFismat = genfis1(trainData, numMFs, mfType);
            outFismat = anfis(trainData, inFismat, epoch);
            writefis(outFismat, strcat(pwd, '/../fis_files/',country,'/',league,'/',datfiles(j).name));
            
        end
        
    %end
   
end

exit