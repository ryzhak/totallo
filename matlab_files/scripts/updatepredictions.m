%добавляем postgresql driver
javaaddpath(strcat(pwd,'/../postgresql-9.4-1202.jdbc4.jar'))

%подключаемся к БД
datasourcename = 'totallo'; %Name of database
driver ='org.postgresql.Driver'; %Driver for postgres in MATLAB
databaseurl = 'jdbc:postgresql://localhost:5433/totallo'; %URL for postgres jdbc in MATLAB
password = '123574896'; %Set password
conn = database(datasourcename,'postgres',password,driver,databaseurl);

%выбираем все лиги
sql = exec(conn,strcat('SELECT * FROM league'));
sql = fetch(sql);
leagues = sql.data;
numLeagues = length(leagues);
%перебираем все лиги
for i = 1:numLeagues
     
    %находим название страны на английском маленькими буквами
    sql = exec(conn, strcat('SELECT eng_name FROM country WHERE id =', num2str(leagues{i,4})));
    sql = fetch(sql);
    country = sql.data;
    country = lower(country{1,1});
    
    %тестируем на РФПЛ
    %if leagues{i,1} == 21 %|| leagues{i,1} == 3 || leagues{i,1} == 5 || leagues{i,1} == 7 || leagues{i,1} == 9
        
        %пропускаем чемпионат азербайджана
        if leagues{i,1} == 20
            continue
        end
        
        %if leagues{i,1} <= 20
        %    continue
        %end
        
    
        %находим все матчи данной лиги
        sql = exec(conn,strcat('SELECT * FROM match WHERE league_id =',num2str(leagues{i,1})));
        sql = fetch(sql);
        matches = sql.data;
        numMatches = length(matches);
        
        %перебираем все матчи
        for j = 1:numMatches
            %disp(matches{j,1})
            
            %находим англ имя домашней команды без пробелов
            sql = exec(conn,strcat('SELECT eng_name FROM team WHERE id =',num2str(matches{j,2})));
            sql = fetch(sql);
            homeTeamName = sql.data;
            homeTeamName = strrep(homeTeamName{1,1},' ','');

            %находим статистику гостей

            %для атакующей модели дома
            %пропущенные мячи в гостях среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 7'));
            sql = fetch(sql);
            p1 = sql.data;
            p1 = p1{1,1};

            %удары по воротам(соперник) среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 18'));
            sql = fetch(sql);
            p2 = sql.data;
            p2 = p2{1,1};

            %удары в створ(соперник) среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 20'));
            sql = fetch(sql);
            p3 = sql.data;
            p3 = p3{1,1};

            %реализация ударов(соперник) среднее
            sql = exec(conn,strcat('SELECT away_sum FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 24'));
            sql = fetch(sql);
            p4 = sql.data;
            p4 = strrep(p4{1,1},'%','');

            %угловые(соперник) среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 28'));
            sql = fetch(sql);
            p5 = sql.data;
            p5 = p5{1,1};
            disp(strcat('p1:',p1,' p2:',p2,' p3:',p3,' p4:',p4,' p5:',p5))
            fisAttackHome = readfis(strcat(pwd,'/../fis_files/',country,'/',strrep(lower(leagues{i,3}),' ',''),'/',homeTeamName,'_ATTACK_HOME.dat.fis'));
            disp(fisAttackHome)
            predictGoalsHome = round(evalfis([str2double(p1) str2double(p2) str2double(p3) str2double(p4) str2double(p5)],fisAttackHome));
            if predictGoalsHome < 0
                predictGoalsHome = 0;
            end


            %для защитной модели дома
            %забитые мячи в гостях среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 6'));
            sql = fetch(sql);
            p1 = sql.data;
            p1 = p1{1,1};

            %удары по воротам в гостях среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 17'));
            sql = fetch(sql);
            p2 = sql.data;
            p2 = p2{1,1};

            %удары в створ в гостях среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 19'));
            sql = fetch(sql);
            p3 = sql.data;
            p3 = p3{1,1};

            %реализация ударов в гостях среднее
            sql = exec(conn,strcat('SELECT away_sum FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 23'));
            sql = fetch(sql);
            p4 = sql.data;
            p4 = strrep(p4{1,1},'%','');

            %угловые в гостях среднее
            sql = exec(conn,strcat('SELECT away_avg FROM stat WHERE team_id = ',num2str(matches{j,3}),' AND stat_param_id = 27'));
            sql = fetch(sql);
            p5 = sql.data;
            p5 = p5{1,1};

            fisDefendHome = readfis(strcat(pwd,'/../fis_files/',country,'/',strrep(lower(leagues{i,3}),' ',''),'/',homeTeamName,'_DEFEND_HOME.dat.fis'));
            predictGoalsAway = round(evalfis([str2double(p1) str2double(p2) str2double(p3) str2double(p4) str2double(p5)],fisDefendHome));
            if predictGoalsAway < 0
                predictGoalsAway = 0;
            end

            %обновляем БД нашими прогнозами
            colnames = {'predict_goals_home','predict_goals_away'};
            exdata = {predictGoalsHome,predictGoalsAway};
            update(conn, 'match', colnames, exdata, strcat('where id = ',num2str(matches{j,1})));
            
            disp(j)
            
        end
        
        
    %end
    
end

exit
