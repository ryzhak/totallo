<?php

use yii\db\Schema;
use yii\db\Migration;

class m150915_085047_basic_tables_and_data extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable("season", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING
        ]);

        $this->createTable("country", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING,
            "eng_name" => Schema::TYPE_STRING
        ]);

        $this->createTable("league", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING,
            "eng_name" => Schema::TYPE_STRING,
            "country_id" => Schema::TYPE_INTEGER,
            "season_id" => Schema::TYPE_INTEGER,
            "championat_table_url" => Schema::TYPE_STRING,
            "marathon_url" => Schema::TYPE_STRING
        ]);
        $this->addForeignKey("league_country", 'league', 'country_id', 'country', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("league_season", 'league', 'season_id', 'season', 'id', 'CASCADE', 'CASCADE');

        $this->createTable("team", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING,
            "eng_name" => Schema::TYPE_STRING,
            "league_id" => Schema::TYPE_INTEGER
        ]);
        $this->addForeignKey("team_league", 'team', 'league_id', 'league', 'id', 'CASCADE', 'CASCADE');

        $this->createTable("stat_param", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING
        ]);

        $this->createTable("stat", [
            "id" => Schema::TYPE_PK,
            "team_id" => Schema::TYPE_INTEGER,
            "stat_param_id" => Schema::TYPE_INTEGER,
            "all_sum" => Schema::TYPE_STRING,
            "all_avg" => Schema::TYPE_STRING,
            "home_sum" => Schema::TYPE_STRING,
            "home_avg" => Schema::TYPE_STRING,
            "away_sum" => Schema::TYPE_STRING,
            "away_avg" => Schema::TYPE_STRING
        ]);

        $this->createTable("coef_param", [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING
        ]);

        $this->createTable("match", [
            "id" => Schema::TYPE_PK,
            "home_team_id" => Schema::TYPE_INTEGER,
            "away_team_id" => Schema::TYPE_INTEGER,
            "league_id" => Schema::TYPE_INTEGER,
            "tour_number" => Schema::TYPE_INTEGER,
            "home_team_goals" => Schema::TYPE_STRING,
            "away_team_goals" => Schema::TYPE_STRING,
            "unix_date" => Schema::TYPE_BIGINT,
            "predict_goals_home" => Schema::TYPE_INTEGER,
            "predict_goals_away" => Schema::TYPE_INTEGER
        ]);
        $this->addForeignKey("match_home_team", 'match', 'home_team_id', 'team', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("match_away_team", 'match', 'away_team_id', 'team', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("match_league", 'match', 'league_id', 'league', 'id', 'CASCADE', 'CASCADE');

        $this->createTable("coef", [
            "id" => Schema::TYPE_PK,
            "match_id" => Schema::TYPE_INTEGER,
            "coef_param_id" => Schema::TYPE_INTEGER,
            "value" => Schema::TYPE_FLOAT
        ]);
        $this->addForeignKey("coef_param", 'coef', 'coef_param_id', 'coef_param', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("coef_match", 'coef', 'match_id', 'match', 'id', 'CASCADE', 'CASCADE');

        $this->createTable("express", [
            "id" => Schema::TYPE_PK,
            "unix_date" => Schema::TYPE_INTEGER,
            "league_id" => Schema::TYPE_INTEGER
        ]);

        $this->createTable("express_part", [
            "id" => Schema::TYPE_PK,
            "match_id" => Schema::TYPE_INTEGER,
            "express_id" => Schema::TYPE_INTEGER,
            "coef_param_id" => Schema::TYPE_INTEGER,
            "predict_goals_home" => Schema::TYPE_INTEGER,
            "predict_goals_away" => Schema::TYPE_INTEGER
        ]);
        $this->addForeignKey("express_part_match", 'express_part', 'match_id', 'match', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("part_express", 'express_part', 'express_id', 'express', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("express_part_coef_param", 'express_part', 'coef_param_id', 'coef_param', 'id', 'CASCADE', 'CASCADE');

        /* Initial data */
        $this->batchInsert("season", ["id", "name"],[
            [1, "2015/2016"]
        ]);

        $this->batchInsert("country", ["id", "name", "eng_name"],[
            [1, "Россия", "Russia"],
            [2, "Англия", "England"],
            [3, "Испания", "Spain"],
            [4, "Италия", "Italy"],
            [5, "Германия", "Germany"],
            [6, "Украина", "Ukraine"],
            [7, "Франция", "France"],
            [8, "Бразилия", "Brazil"],
            [9, "Аргентина", "Argentina"],
            [10, "Нидерланды", "Netherlands"],
            [11, "Португалия", "Portugal"],
            [12, "Турция", "Turkey"],
            [13, "Бельгия", "Belgium"],
            [14, "Австрия", "Austria"],
            [15, "Азербайджан", "Azerbaijan"],
            [16, "Болгария", "Bulgaria"],
            [17, "Греция", "Greece"],
            [18, "Дания", "Denmark"],
            [19, "Польша", "Poland"],
            [20, "Хорватия", "Croatia"],
            [21, "Чехия", "Czech Republic"],
            [22, "Шотландия", "Scotland"],
        ]);

        $this->batchInsert("league", ["id", "name", "eng_name", "country_id", "season_id", "championat_table_url", "marathon_url"],[
            [1, "Премьер-лига", "Premier League", 1, 1, "http://www.championat.com/football/_russiapl/1299/table/all.html", "https://www.betmarathon.com/en/betting/Football/Russia/Premier+League"],
            [2, "ФНЛ", "FNL", 1, 1, "http://www.championat.com/football/_russia1d/1335/table/all.html", "https://www.betmarathon.com/en/betting/Football/Russia/FNL"],
            [3, "Премьер-лига", "Premier League", 2, 1, "http://www.championat.com/football/_england/1323/table/all.html", "https://www.betmarathon.com/en/betting/Football/England/Premier+League"],
            [4, "Чемпионшип", "Championship", 2, 1, "http://www.championat.com/football/_england/1597/table/all/group.html", "https://www.betmarathon.com/en/betting/Football/England/Championship"],
            [5, "Примера", "Primera Division", 3, 1, "http://www.championat.com/football/_spain/1331/table/all.html", "https://www.betmarathon.com/en/popular/Football/Spain/Primera+Division"],
            [6, "Сегунда", "Segunda Division", 3, 1, "http://www.championat.com/football/_spain/1607/table/all/group.html", "https://www.betmarathon.com/en/popular/Football/Spain/Segunda+Division"],
            [7, "Серия A", "Serie A", 4, 1, "http://www.championat.com/football/_italy/1329/table/all.html", "https://www.betmarathon.com/en/popular/Football/Italy/Serie+A"],
            [8, "Серия B", "Serie B", 4, 1, "http://www.championat.com/football/_italy/1621/table/all/group.html", "https://www.betmarathon.com/en/popular/Football/Italy/Serie+B"],
            [9, "Бундеслига", "Bundesliga", 5, 1, "http://www.championat.com/football/_germany/1327/table/all.html", "https://www.betmarathon.com/en/popular/Football/Germany/Bundesliga"],
            [10, "Вторая Бундеслига", "Bundesliga 2", 5, 1, "http://www.championat.com/football/_germany/1577/table/all.html", "https://www.betmarathon.com/en/popular/Football/Germany/Bundesliga+2"],
            [11, "Премьер-лига", "Premier League", 6, 1, "http://www.championat.com/football/_ukraine/1354/table/all.html", "https://www.betmarathon.com/en/popular/Football/Ukraine/Premier+League"],
            [12, "Лига 1", "Ligue 1", 7, 1, "http://www.championat.com/football/_france/1333/table/all.html", "https://www.betmarathon.com/en/popular/Football/France/Ligue+1"],
            [13, "Серия A", "Serie A", 8, 1, "http://www.championat.com/football/_southamerica/1255/table/all.html", "https://www.betmarathon.com/en/popular/Football/Brazil/Serie+A"],
            [14, "Примера", "Primera Division", 9, 1, "http://www.championat.com/football/_southamerica/1267/table/all.html", "https://www.betmarathon.com/en/popular/Football/Argentina/Primera+Division"],
            [15, "Эредивизия", "Eredivisie", 10, 1, "http://www.championat.com/football/_other/1388/table/all.html", "https://www.betmarathon.com/en/popular/Football/Netherlands/Eredivisie"],
            [16, "Примейра", "Primeira Liga", 11, 1, "http://www.championat.com/football/_other/1378/table/all.html", "https://www.betmarathon.com/en/popular/Football/Portugal/Primeira+Liga"],
            [17, "Суперлига", "Super League", 12, 1, "http://www.championat.com/football/_other/1402/table/all.html", "https://www.betmarathon.com/en/popular/Football/Turkey/Super+League"],
            [18, "Лига Жюпиле", "Jupiler Pro League", 13, 1, "http://www.championat.com/football/_other/1408/table/all.html", "https://www.betmarathon.com/en/popular/Football/Belgium/Jupiler+Pro+League"],
            [19, "Бундеслига", "Bundesliga", 14, 1, "http://www.championat.com/football/_other/1420/table/all.html", "https://www.betmarathon.com/en/popular/Football/Austria/Bundesliga"],
            [20, "Премьер-лига", "Premier League", 15, 1, "http://www.championat.com/football/_other/1424/table/all.html", "https://www.betmarathon.com/en/popular/Football/Azerbaijan/Premier+League"],
            [21, "Группа A", "A PFG", 16, 1, "http://www.championat.com/football/_other/1436/table/all.html", "https://www.betmarathon.com/en/popular/Football/Bulgaria/A+PFG"],
            [22, "Суперлига", "Super League", 17, 1, "http://www.championat.com/football/_other/1444/table/all/group.html", "https://www.betmarathon.com/en/popular/Football/Greece/Super+League"],
            [23, "Суперлига", "Superliga", 18, 1, "http://www.championat.com/football/_other/1452/table/all.html", "https://www.betmarathon.com/en/popular/Football/Denmark/Superliga"],
            [24, "Экстракласса", "Ekstraklasa", 19, 1, "http://www.championat.com/football/_other/1494/table/all.html", "https://www.betmarathon.com/en/popular/Football/Poland/Ekstraklasa"],
            [25, "Первая лига", "League 1", 20, 1, "http://www.championat.com/football/_other/1516/table/all.html", "https://www.betmarathon.com/en/popular/Football/Croatia/1st+League"],
            [26, "Высшая лига", "Synot Liga", 21, 1, "http://www.championat.com/football/_other/1530/table/all.html", "https://www.betmarathon.com/en/popular/Football/Czech+Republic/Synot+Liga"],
            [27, "Премьер-лига", "Premiership", 22, 1, "http://www.championat.com/football/_other/1540/table/all.html", "https://www.betmarathon.com/en/popular/Football/Scotland/Premiership"],
        ]);

        $this->batchInsert("coef_param", ["id", "name"],[
            [1, "Тотал больше 1.5"],
            [2, "Тотал больше 2"]
        ]);

    }

    public function safeDown()
    {
        $this->dropTable("express_part");
        $this->dropTable("express");
        $this->dropTable("coef");
        $this->dropTable("coef_param");
        $this->dropTable("match");
        $this->dropTable("team");
        $this->dropTable("league");
        $this->dropTable("stat");
        $this->dropTable("stat_param");
        $this->dropTable("season");
        $this->dropTable("country");
    }

}
