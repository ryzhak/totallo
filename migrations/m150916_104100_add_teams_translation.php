<?php

use yii\db\Schema;
use yii\db\Migration;

class m150916_104100_add_teams_translation extends Migration
{
    public function safeUp()
    {
        //Россия РФПЛ
        $this->update("team", ["eng_name" => "Spartak Moscow"], ["id" => 1]);
        $this->update("team", ["eng_name" => "Ufa"], ["id" => 2]);
        $this->update("team", ["eng_name" => "CSKA Moscow"], ["id" => 3]);
        $this->update("team", ["eng_name" => "Rubin Kazan"], ["id" => 4]);
        $this->update("team", ["eng_name" => "Rostov"], ["id" => 5]);
        $this->update("team", ["eng_name" => "Terek"], ["id" => 6]);
        $this->update("team", ["eng_name" => "Zenit St Petersburg"], ["id" => 7]);
        $this->update("team", ["eng_name" => "Dynamo Moscow"], ["id" => 8]);
        $this->update("team", ["eng_name" => "Mordoviya"], ["id" => 9]);
        $this->update("team", ["eng_name" => "Lokomotiv Moscow"], ["id" => 10]);
        $this->update("team", ["eng_name" => "Anzhi"], ["id" => 11]);
        $this->update("team", ["eng_name" => "Krylia Sovetov"], ["id" => 12]);
        $this->update("team", ["eng_name" => "Amkar Perm"], ["id" => 13]);
        $this->update("team", ["eng_name" => "Krasnodar"], ["id" => 14]);
        $this->update("team", ["eng_name" => "Kuban"], ["id" => 15]);
        $this->update("team", ["eng_name" => "Ural"], ["id" => 16]);

        //Россия ФНЛ
        $this->update("team", ["eng_name" => "Luch-Energiya"], ["id" => 17]);
        $this->update("team", ["eng_name" => "SKA-Energiya"], ["id" => 18]);
        $this->update("team", ["eng_name" => "Tosno"], ["id" => 19]);
        $this->update("team", ["eng_name" => "Fakel"], ["id" => 20]);
        $this->update("team", ["eng_name" => "Tom Tomsk"], ["id" => 21]);
        $this->update("team", ["eng_name" => "Spartak-2 Moscow"], ["id" => 22]);
        $this->update("team", ["eng_name" => "Yenisey"], ["id" => 23]);
        $this->update("team", ["eng_name" => "Volga Nizhny Novgorod"], ["id" => 24]);
        $this->update("team", ["eng_name" => "Tyumen"], ["id" => 25]);
        $this->update("team", ["eng_name" => "Sokol Saratov"], ["id" => 26]);
        $this->update("team", ["eng_name" => "Arsenal Tula"], ["id" => 27]);
        $this->update("team", ["eng_name" => "Baykal"], ["id" => 28]);
        $this->update("team", ["eng_name" => "Gazovik"], ["id" => 29]);
        $this->update("team", ["eng_name" => "KAMAZ Naberezhnye Chelny"], ["id" => 30]);
        $this->update("team", ["eng_name" => "Shinnik"], ["id" => 31]);
        $this->update("team", ["eng_name" => "Baltika"], ["id" => 32]);
        $this->update("team", ["eng_name" => "Volgar Astrakhan"], ["id" => 33]);
        $this->update("team", ["eng_name" => "Sibir"], ["id" => 34]);
        $this->update("team", ["eng_name" => "Zenit-2 St Petersburg"], ["id" => 35]);
        $this->update("team", ["eng_name" => "Torpedo Armavir"], ["id" => 36]);

        //Англия премьер-лига
        $this->update("team", ["eng_name" => "Manchester United"], ["id" => 37]);
        $this->update("team", ["eng_name" => "Tottenham Hotspur"], ["id" => 38]);
        $this->update("team", ["eng_name" => "Bournemouth"], ["id" => 39]);
        $this->update("team", ["eng_name" => "Aston Villa"], ["id" => 40]);
        $this->update("team", ["eng_name" => "Norwich City"], ["id" => 41]);
        $this->update("team", ["eng_name" => "Crystal Palace"], ["id" => 42]);
        $this->update("team", ["eng_name" => "Leicester City"], ["id" => 43]);
        $this->update("team", ["eng_name" => "Sunderland"], ["id" => 44]);
        $this->update("team", ["eng_name" => "Everton"], ["id" => 45]);
        $this->update("team", ["eng_name" => "Watford"], ["id" => 46]);
        $this->update("team", ["eng_name" => "Chelsea"], ["id" => 47]);
        $this->update("team", ["eng_name" => "Swansea City"], ["id" => 48]);
        $this->update("team", ["eng_name" => "Newcastle United"], ["id" => 49]);
        $this->update("team", ["eng_name" => "Southampton"], ["id" => 50]);
        $this->update("team", ["eng_name" => "Arsenal"], ["id" => 51]);
        $this->update("team", ["eng_name" => "West Ham United"], ["id" => 52]);
        $this->update("team", ["eng_name" => "Stoke City"], ["id" => 53]);
        $this->update("team", ["eng_name" => "Liverpool"], ["id" => 54]);
        $this->update("team", ["eng_name" => "West Bromwich Albion"], ["id" => 55]);
        $this->update("team", ["eng_name" => "Manchester City"], ["id" => 56]);

        //Англия чемпионшип
        $this->update("team", ["eng_name" => "Brighton & Hove Albion"], ["id" => 57]);
        $this->update("team", ["eng_name" => "Nottingham Forest"], ["id" => 58]);
        $this->update("team", ["eng_name" => "Cardiff City"], ["id" => 59]);
        $this->update("team", ["eng_name" => "Fulham"], ["id" => 60]);
        $this->update("team", ["eng_name" => "Leeds United"], ["id" => 61]);
        $this->update("team", ["eng_name" => "Burnley"], ["id" => 62]);
        $this->update("team", ["eng_name" => "Birmingham City"], ["id" => 63]);
        $this->update("team", ["eng_name" => "Reading"], ["id" => 64]);
        $this->update("team", ["eng_name" => "Blackburn Rovers"], ["id" => 65]);
        $this->update("team", ["eng_name" => "Wolverhampton Wanderers"], ["id" => 66]);
        $this->update("team", ["eng_name" => "Bolton Wanderers"], ["id" => 67]);
        $this->update("team", ["eng_name" => "Derby County"], ["id" => 68]);
        $this->update("team", ["eng_name" => "Brentford"], ["id" => 69]);
        $this->update("team", ["eng_name" => "Ipswich Town"], ["id" => 70]);
        $this->update("team", ["eng_name" => "Charlton Athletic"], ["id" => 71]);
        $this->update("team", ["eng_name" => "Queens Park Rangers"], ["id" => 72]);
        $this->update("team", ["eng_name" => "Hull City"], ["id" => 73]);
        $this->update("team", ["eng_name" => "Huddersfield Town"], ["id" => 74]);
        $this->update("team", ["eng_name" => "Rotherham United"], ["id" => 75]);
        $this->update("team", ["eng_name" => "Milton Keynes Dons"], ["id" => 76]);
        $this->update("team", ["eng_name" => "Sheffield Wednesday"], ["id" => 77]);
        $this->update("team", ["eng_name" => "Bristol City"], ["id" => 78]);
        $this->update("team", ["eng_name" => "Preston North End"], ["id" => 79]);
        $this->update("team", ["eng_name" => "Middlesbrough"], ["id" => 80]);

        //Испания примера
        $this->update("team", ["eng_name" => "Malaga"], ["id" => 81]);
        $this->update("team", ["eng_name" => "Sevilla"], ["id" => 82]);
        $this->update("team", ["eng_name" => "Espanyol"], ["id" => 83]);
        $this->update("team", ["eng_name" => "Getafe"], ["id" => 84]);
        $this->update("team", ["eng_name" => "Deportivo de la Coruna"], ["id" => 85]);
        $this->update("team", ["eng_name" => "Real Sociedad"], ["id" => 86]);
        $this->update("team", ["eng_name" => "Atletico Madrid"], ["id" => 87]);
        $this->update("team", ["eng_name" => "Las Palmas"], ["id" => 88]);
        $this->update("team", ["eng_name" => "Rayo Vallecano"], ["id" => 89]);
        $this->update("team", ["eng_name" => "Valencia"], ["id" => 90]);
        $this->update("team", ["eng_name" => "Athletic Bilbao"], ["id" => 91]);
        $this->update("team", ["eng_name" => "Barcelona"], ["id" => 92]);
        $this->update("team", ["eng_name" => "Sporting Gijon"], ["id" => 93]);
        $this->update("team", ["eng_name" => "Real Madrid"], ["id" => 94]);
        $this->update("team", ["eng_name" => "Levante"], ["id" => 95]);
        $this->update("team", ["eng_name" => "Celta de Vigo"], ["id" => 96]);
        $this->update("team", ["eng_name" => "Betis"], ["id" => 97]);
        $this->update("team", ["eng_name" => "Villarreal"], ["id" => 98]);
        $this->update("team", ["eng_name" => "Granada"], ["id" => 99]);
        $this->update("team", ["eng_name" => "Eibar"], ["id" => 100]);

        //Испания сегунда
        $this->update("team", ["eng_name" => "Llagostera"], ["id" => 101]);
        $this->update("team", ["eng_name" => "Osasuna"], ["id" => 102]);
        $this->update("team", ["eng_name" => "Alcorcon"], ["id" => 103]);
        $this->update("team", ["eng_name" => "Mallorca"], ["id" => 104]);
        $this->update("team", ["eng_name" => "Huesca"], ["id" => 105]);
        $this->update("team", ["eng_name" => "Alaves"], ["id" => 106]);
        $this->update("team", ["eng_name" => "Cordoba"], ["id" => 107]);
        $this->update("team", ["eng_name" => "Valladolid"], ["id" => 108]);
        $this->update("team", ["eng_name" => "Mirandes"], ["id" => 109]);
        $this->update("team", ["eng_name" => "Zaragoza"], ["id" => 110]);
        $this->update("team", ["eng_name" => "Real Oviedo"], ["id" => 111]);
        $this->update("team", ["eng_name" => "Lugo"], ["id" => 112]);
        $this->update("team", ["eng_name" => "Almeria"], ["id" => 113]);
        $this->update("team", ["eng_name" => "Leganes"], ["id" => 114]);
        $this->update("team", ["eng_name" => "Numancia"], ["id" => 115]);
        $this->update("team", ["eng_name" => "Tenerife"], ["id" => 116]);
        $this->update("team", ["eng_name" => "Ponferradina"], ["id" => 117]);
        $this->update("team", ["eng_name" => "Elche"], ["id" => 118]);
        $this->update("team", ["eng_name" => "Gimnastic de Tarragona"], ["id" => 119]);
        $this->update("team", ["eng_name" => "Albacete"], ["id" => 120]);
        $this->update("team", ["eng_name" => "Athletic Bilbao B"], ["id" => 121]);
        $this->update("team", ["eng_name" => "Girona"], ["id" => 122]);

        //Италия серия А
        $this->update("team", ["eng_name" => "Verona"], ["id" => 123]);
        $this->update("team", ["eng_name" => "Roma"], ["id" => 124]);
        $this->update("team", ["eng_name" => "Lazio"], ["id" => 125]);
        $this->update("team", ["eng_name" => "Bologna"], ["id" => 126]);
        $this->update("team", ["eng_name" => "Juventus"], ["id" => 127]);
        $this->update("team", ["eng_name" => "Udinese"], ["id" => 128]);
        $this->update("team", ["eng_name" => "Empoli"], ["id" => 129]);
        $this->update("team", ["eng_name" => "Chievo"], ["id" => 130]);
        $this->update("team", ["eng_name" => "Fiorentina"], ["id" => 131]);
        $this->update("team", ["eng_name" => "Milan"], ["id" => 132]);
        $this->update("team", ["eng_name" => "Frosinone"], ["id" => 133]);
        $this->update("team", ["eng_name" => "Torino"], ["id" => 134]);
        $this->update("team", ["eng_name" => "Inter"], ["id" => 135]);
        $this->update("team", ["eng_name" => "Atalanta"], ["id" => 136]);
        $this->update("team", ["eng_name" => "Palermo"], ["id" => 137]);
        $this->update("team", ["eng_name" => "Genoa"], ["id" => 138]);
        $this->update("team", ["eng_name" => "Sampdoria"], ["id" => 139]);
        $this->update("team", ["eng_name" => "Carpi"], ["id" => 140]);
        $this->update("team", ["eng_name" => "Sassuolo"], ["id" => 141]);
        $this->update("team", ["eng_name" => "Napoli"], ["id" => 142]);

        //Италия серия B
        $this->update("team", ["eng_name" => "Cesena"], ["id" => 143]);
        $this->update("team", ["eng_name" => "Brescia"], ["id" => 144]);
        $this->update("team", ["eng_name" => "Livorno"], ["id" => 145]);
        $this->update("team", ["eng_name" => "Pescara"], ["id" => 146]);
        $this->update("team", ["eng_name" => "Modena"], ["id" => 147]);
        $this->update("team", ["eng_name" => "Vicenza"], ["id" => 148]);
        $this->update("team", ["eng_name" => "Novara"], ["id" => 149]);
        $this->update("team", ["eng_name" => "Latina"], ["id" => 150]);
        $this->update("team", ["eng_name" => "Perugia"], ["id" => 151]);
        $this->update("team", ["eng_name" => "Como"], ["id" => 152]);
        $this->update("team", ["eng_name" => "Pro Vercelli"], ["id" => 153]);
        $this->update("team", ["eng_name" => "Lanciano"], ["id" => 154]);
        $this->update("team", ["eng_name" => "Salernitana"], ["id" => 155]);
        $this->update("team", ["eng_name" => "Avellino"], ["id" => 156]);
        $this->update("team", ["eng_name" => "Trapani"], ["id" => 157]);
        $this->update("team", ["eng_name" => "Ternana"], ["id" => 158]);
        $this->update("team", ["eng_name" => "Bari"], ["id" => 159]);
        $this->update("team", ["eng_name" => "Spezia"], ["id" => 160]);
        $this->update("team", ["eng_name" => "Cagliari"], ["id" => 161]);
        $this->update("team", ["eng_name" => "Crotone"], ["id" => 162]);
        $this->update("team", ["eng_name" => "Ascoli"], ["id" => 163]);
        $this->update("team", ["eng_name" => "Virtus Entella"], ["id" => 164]);

        //Германия бундеслига
        $this->update("team", ["eng_name" => "Bayern Munich"], ["id" => 165]);
        $this->update("team", ["eng_name" => "Hamburger"], ["id" => 166]);
        $this->update("team", ["eng_name" => "Bayer 04 Leverkusen"], ["id" => 167]);
        $this->update("team", ["eng_name" => "1899 Hoffenheim"], ["id" => 168]);
        $this->update("team", ["eng_name" => "Augsburg"], ["id" => 169]);
        $this->update("team", ["eng_name" => "Hertha Berlin"], ["id" => 170]);
        $this->update("team", ["eng_name" => "Werder Bremen"], ["id" => 171]);
        $this->update("team", ["eng_name" => "Schalke 04"], ["id" => 172]);
        $this->update("team", ["eng_name" => "Mainz 05"], ["id" => 173]);
        $this->update("team", ["eng_name" => "Ingolstadt 04"], ["id" => 174]);
        $this->update("team", ["eng_name" => "Darmstadt 98"], ["id" => 175]);
        $this->update("team", ["eng_name" => "Hannover 96"], ["id" => 176]);
        $this->update("team", ["eng_name" => "Borussia Dortmund"], ["id" => 177]);
        $this->update("team", ["eng_name" => "Borussia Monchengladbach"], ["id" => 178]);
        $this->update("team", ["eng_name" => "Wolfsburg"], ["id" => 179]);
        $this->update("team", ["eng_name" => "Eintracht Frankfurt"], ["id" => 180]);
        $this->update("team", ["eng_name" => "Stuttgart"], ["id" => 181]);
        $this->update("team", ["eng_name" => "Koln"], ["id" => 182]);

        //Германия бундеслига 2
        $this->update("team", ["eng_name" => "Duisburg"], ["id" => 183]);
        $this->update("team", ["eng_name" => "Kaiserslautern"], ["id" => 184]);
        $this->update("team", ["eng_name" => "Greuther Furth"], ["id" => 185]);
        $this->update("team", ["eng_name" => "Karlsruher"], ["id" => 186]);
        $this->update("team", ["eng_name" => "St Pauli"], ["id" => 187]);
        $this->update("team", ["eng_name" => "Arminia Bielefeld"], ["id" => 188]);
        $this->update("team", ["eng_name" => "FSV Frankfurt"], ["id" => 189]);
        $this->update("team", ["eng_name" => "RB Leipzig"], ["id" => 190]);
        $this->update("team", ["eng_name" => "Paderborn 07"], ["id" => 191]);
        $this->update("team", ["eng_name" => "Bochum"], ["id" => 192]);
        $this->update("team", ["eng_name" => "Eintracht Braunschweig"], ["id" => 193]);
        $this->update("team", ["eng_name" => "Sandhausen"], ["id" => 194]);
        $this->update("team", ["eng_name" => "Union Berlin"], ["id" => 195]);
        $this->update("team", ["eng_name" => "Fortuna Dusseldorf"], ["id" => 196]);
        $this->update("team", ["eng_name" => "Heidenheim"], ["id" => 197]);
        $this->update("team", ["eng_name" => "1860 Munich"], ["id" => 198]);
        $this->update("team", ["eng_name" => "Freiburg"], ["id" => 199]);
        $this->update("team", ["eng_name" => "Nurnberg"], ["id" => 200]);

        //Украина премьер-лига
        $this->update("team", ["eng_name" => "Metalurh Zaporizhya"], ["id" => 201]);
        $this->update("team", ["eng_name" => "Zorya Lugansk"], ["id" => 202]);
        $this->update("team", ["eng_name" => "Metalist Kharkiv"], ["id" => 203]);
        $this->update("team", ["eng_name" => "Karpaty"], ["id" => 204]);
        $this->update("team", ["eng_name" => "Olimpik Donetsk"], ["id" => 205]);
        $this->update("team", ["eng_name" => "Chornomorets Odesa"], ["id" => 206]);
        $this->update("team", ["eng_name" => "Vorskla Poltava"], ["id" => 207]);
        $this->update("team", ["eng_name" => "Volyn"], ["id" => 208]);
        $this->update("team", ["eng_name" => "Shakhtar Donetsk"], ["id" => 209]);
        $this->update("team", ["eng_name" => "Oleksandria"], ["id" => 210]);
        $this->update("team", ["eng_name" => "Hoverla Uzhhorod"], ["id" => 211]);
        $this->update("team", ["eng_name" => "Dnipro"], ["id" => 212]);
        $this->update("team", ["eng_name" => "Stal Dniprodzerzhynsk"], ["id" => 213]);
        $this->update("team", ["eng_name" => "Dynamo Kyiv"], ["id" => 214]);

        //Франция лига 1
        $this->update("team", ["eng_name" => "Lille"], ["id" => 215]);
        $this->update("team", ["eng_name" => "Paris Saint-Germain"], ["id" => 216]);
        $this->update("team", ["eng_name" => "Nantes"], ["id" => 217]);
        $this->update("team", ["eng_name" => "Guingamp"], ["id" => 218]);
        $this->update("team", ["eng_name" => "Montpellier"], ["id" => 219]);
        $this->update("team", ["eng_name" => "Angers"], ["id" => 220]);
        $this->update("team", ["eng_name" => "Nice"], ["id" => 221]);
        $this->update("team", ["eng_name" => "Monaco"], ["id" => 222]);
        $this->update("team", ["eng_name" => "Marseille"], ["id" => 223]);
        $this->update("team", ["eng_name" => "Caen"], ["id" => 224]);
        $this->update("team", ["eng_name" => "SC Bastia"], ["id" => 225]);
        $this->update("team", ["eng_name" => "Rennes"], ["id" => 226]);
        $this->update("team", ["eng_name" => "Troyes"], ["id" => 227]);
        $this->update("team", ["eng_name" => "Gazelec Ajaccio"], ["id" => 228]);
        $this->update("team", ["eng_name" => "Bordeaux"], ["id" => 229]);
        $this->update("team", ["eng_name" => "Reims"], ["id" => 230]);
        $this->update("team", ["eng_name" => "Toulouse"], ["id" => 231]);
        $this->update("team", ["eng_name" => "Saint-Etienne"], ["id" => 232]);
        $this->update("team", ["eng_name" => "Lyon"], ["id" => 233]);
        $this->update("team", ["eng_name" => "Lorient"], ["id" => 234]);

        //Бразилия серия А
        $this->update("team", ["eng_name" => "Palmeiras"], ["id" => 235]);
        $this->update("team", ["eng_name" => "Atletico-MG"], ["id" => 236]);
        $this->update("team", ["eng_name" => "Chapecoense"], ["id" => 237]);
        $this->update("team", ["eng_name" => "Coritiba"], ["id" => 238]);
        $this->update("team", ["eng_name" => "Fluminense"], ["id" => 239]);
        $this->update("team", ["eng_name" => "Joinville"], ["id" => 240]);
        $this->update("team", ["eng_name" => "Gremio"], ["id" => 241]);
        $this->update("team", ["eng_name" => "Ponte Preta"], ["id" => 242]);
        $this->update("team", ["eng_name" => "Sao Paulo"], ["id" => 243]);
        $this->update("team", ["eng_name" => "Flamengo"], ["id" => 244]);
        $this->update("team", ["eng_name" => "Cruzeiro-MG"], ["id" => 245]);
        $this->update("team", ["eng_name" => "Corinthians-SP"], ["id" => 246]);
        $this->update("team", ["eng_name" => "Atletico-PR"], ["id" => 247]);
        $this->update("team", ["eng_name" => "Internacional-RS"], ["id" => 248]);
        $this->update("team", ["eng_name" => "Sport Recife"], ["id" => 249]);
        $this->update("team", ["eng_name" => "Figueirense"], ["id" => 250]);
        $this->update("team", ["eng_name" => "Vasco da Gama-RJ"], ["id" => 251]);
        $this->update("team", ["eng_name" => "Goias"], ["id" => 252]);
        $this->update("team", ["eng_name" => "Avai"], ["id" => 253]);
        $this->update("team", ["eng_name" => "Santos"], ["id" => 254]);

        //Аргентина примера
        $this->update("team", ["eng_name" => "Velez Sarsfield"], ["id" => 255]);
        $this->update("team", ["eng_name" => "Aldosivi"], ["id" => 256]);
        $this->update("team", ["eng_name" => "Racing de Avellaneda"], ["id" => 257]);
        $this->update("team", ["eng_name" => "Rosario Central"], ["id" => 258]);
        $this->update("team", ["eng_name" => "San Lorenzo"], ["id" => 259]);
        $this->update("team", ["eng_name" => "Colon de Santa Fe"], ["id" => 260]);
        $this->update("team", ["eng_name" => "Gimnasia La Plata"], ["id" => 261]);
        $this->update("team", ["eng_name" => "Defensa y Justicia"], ["id" => 262]);
        $this->update("team", ["eng_name" => "Godoy Cruz"], ["id" => 263]);
        $this->update("team", ["eng_name" => "San Martin San Juan"], ["id" => 264]);
        $this->update("team", ["eng_name" => "Newells Old Boys"], ["id" => 265]);
        $this->update("team", ["eng_name" => "Independiente Avellaneda"], ["id" => 266]);
        $this->update("team", ["eng_name" => "Banfield"], ["id" => 267]);
        $this->update("team", ["eng_name" => "Temperley"], ["id" => 268]);
        $this->update("team", ["eng_name" => "Crucero del Norte"], ["id" => 269]);
        $this->update("team", ["eng_name" => "Tigre"], ["id" => 270]);
        $this->update("team", ["eng_name" => "Union de Santa Fe"], ["id" => 271]);
        $this->update("team", ["eng_name" => "Huracan"], ["id" => 272]);
        $this->update("team", ["eng_name" => "Quilmes"], ["id" => 273]);
        $this->update("team", ["eng_name" => "Lanus"], ["id" => 274]);
        $this->update("team", ["eng_name" => "Boca Juniors"], ["id" => 275]);
        $this->update("team", ["eng_name" => "Olimpo de Bahia Blanca"], ["id" => 276]);
        $this->update("team", ["eng_name" => "Argentinos Juniors"], ["id" => 277]);
        $this->update("team", ["eng_name" => "Atletico de Rafaela"], ["id" => 278]);
        $this->update("team", ["eng_name" => "Sarmiento Junin"], ["id" => 279]);
        $this->update("team", ["eng_name" => "River Plate"], ["id" => 280]);
        $this->update("team", ["eng_name" => "Arsenal Sarandi"], ["id" => 281]);
        $this->update("team", ["eng_name" => "Estudiantes de La Plata"], ["id" => 282]);
        $this->update("team", ["eng_name" => "Belgrano"], ["id" => 283]);
        $this->update("team", ["eng_name" => "Nueva Chicago"], ["id" => 284]);

        //Нидерланды эрдивизия
        $this->update("team", ["eng_name" => "Roda"], ["id" => 285]);
        $this->update("team", ["eng_name" => "Heracles"], ["id" => 286]);
        $this->update("team", ["eng_name" => "Feyenoord"], ["id" => 287]);
        $this->update("team", ["eng_name" => "Utrecht"], ["id" => 288]);
        $this->update("team", ["eng_name" => "AZ Alkmaar"], ["id" => 289]);
        $this->update("team", ["eng_name" => "Ajax"], ["id" => 290]);
        $this->update("team", ["eng_name" => "Willem II"], ["id" => 291]);
        $this->update("team", ["eng_name" => "Vitesse"], ["id" => 292]);
        $this->update("team", ["eng_name" => "ADO Den Haag"], ["id" => 293]);
        $this->update("team", ["eng_name" => "PSV Eindhoven"], ["id" => 294]);
        $this->update("team", ["eng_name" => "Heerenveen"], ["id" => 295]);
        $this->update("team", ["eng_name" => "De Graafschap"], ["id" => 296]);
        $this->update("team", ["eng_name" => "Groningen"], ["id" => 297]);
        $this->update("team", ["eng_name" => "Twente"], ["id" => 298]);
        $this->update("team", ["eng_name" => "Zwolle"], ["id" => 299]);
        $this->update("team", ["eng_name" => "Cambuur"], ["id" => 300]);
        $this->update("team", ["eng_name" => "NEC Nijmegen"], ["id" => 301]);
        $this->update("team", ["eng_name" => "Excelsior"], ["id" => 302]);

        //Португалия примейра
        $this->update("team", ["eng_name" => "Tondela"], ["id" => 303]);
        $this->update("team", ["eng_name" => "Sporting Lisboa"], ["id" => 304]);
        $this->update("team", ["eng_name" => "Belenenses"], ["id" => 305]);
        $this->update("team", ["eng_name" => "Rio Ave"], ["id" => 306]);
        $this->update("team", ["eng_name" => "Porto"], ["id" => 307]);
        $this->update("team", ["eng_name" => "Vitoria Guimaraes"], ["id" => 308]);
        $this->update("team", ["eng_name" => "Vitoria Setubal"], ["id" => 309]);
        $this->update("team", ["eng_name" => "Boavista"], ["id" => 310]);
        $this->update("team", ["eng_name" => "Uniao Madeira"], ["id" => 311]);
        $this->update("team", ["eng_name" => "Maritimo"], ["id" => 312]);
        $this->update("team", ["eng_name" => "Moreirense"], ["id" => 313]);
        $this->update("team", ["eng_name" => "Arouca"], ["id" => 314]);
        $this->update("team", ["eng_name" => "Sporting Braga"], ["id" => 315]);
        $this->update("team", ["eng_name" => "Nacional Madeira"], ["id" => 316]);
        $this->update("team", ["eng_name" => "Benfica"], ["id" => 317]);
        $this->update("team", ["eng_name" => "Estoril"], ["id" => 318]);
        $this->update("team", ["eng_name" => "Pacos de Ferreira"], ["id" => 319]);
        $this->update("team", ["eng_name" => "Academica Coimbra"], ["id" => 320]);

        //Турция суперлига
        $this->update("team", ["eng_name" => "Fenerbahce"], ["id" => 321]);
        $this->update("team", ["eng_name" => "Eskisehirspor"], ["id" => 322]);
        $this->update("team", ["eng_name" => "Trabzonspor"], ["id" => 323]);
        $this->update("team", ["eng_name" => "Bursaspor"], ["id" => 324]);
        $this->update("team", ["eng_name" => "Istanbul Basaksehir"], ["id" => 325]);
        $this->update("team", ["eng_name" => "Antalyaspor"], ["id" => 326]);
        $this->update("team", ["eng_name" => "Sivasspor"], ["id" => 327]);
        $this->update("team", ["eng_name" => "Galatasaray"], ["id" => 328]);
        $this->update("team", ["eng_name" => "Konyaspor"], ["id" => 329]);
        $this->update("team", ["eng_name" => "Akhisar Belediyespor"], ["id" => 330]);
        $this->update("team", ["eng_name" => "Osmanlispor"], ["id" => 331]);
        $this->update("team", ["eng_name" => "Kayserispor"], ["id" => 332]);
        $this->update("team", ["eng_name" => "Gaziantepspor"], ["id" => 333]);
        $this->update("team", ["eng_name" => "Kasimpasa"], ["id" => 334]);
        $this->update("team", ["eng_name" => "Mersin Idmanyurdu"], ["id" => 335]);
        $this->update("team", ["eng_name" => "Besiktas"], ["id" => 336]);
        $this->update("team", ["eng_name" => "Genclerbirligi"], ["id" => 337]);
        $this->update("team", ["eng_name" => "Caykur Rizespor"], ["id" => 338]);

        //Бельгия лига жюпиле
        $this->update("team", ["eng_name" => "St Truiden"], ["id" => 339]);
        $this->update("team", ["eng_name" => "Brugge"], ["id" => 340]);
        $this->update("team", ["eng_name" => "Kortrijk"], ["id" => 341]);
        $this->update("team", ["eng_name" => "Standard"], ["id" => 342]);
        $this->update("team", ["eng_name" => "Genk"], ["id" => 343]);
        $this->update("team", ["eng_name" => "Oud-Heverlee Leuven"], ["id" => 344]);
        $this->update("team", ["eng_name" => "Oostende"], ["id" => 345]);
        $this->update("team", ["eng_name" => "Mechelen"], ["id" => 346]);
        $this->update("team", ["eng_name" => "Zulte-Waregem"], ["id" => 347]);
        $this->update("team", ["eng_name" => "Lokeren"], ["id" => 348]);
        $this->update("team", ["eng_name" => "Westerlo"], ["id" => 349]);
        $this->update("team", ["eng_name" => "Gent"], ["id" => 350]);
        $this->update("team", ["eng_name" => "Anderlecht"], ["id" => 351]);
        $this->update("team", ["eng_name" => "Waasland-Beveren"], ["id" => 352]);
        $this->update("team", ["eng_name" => "Charleroi"], ["id" => 353]);
        $this->update("team", ["eng_name" => "Mouscron-Peruwelz"], ["id" => 354]);

        //Австрия бундеслига
        $this->update("team", ["eng_name" => "Mattersburg"], ["id" => 355]);
        $this->update("team", ["eng_name" => "Red Bull Salzburg"], ["id" => 356]);
        $this->update("team", ["eng_name" => "Sturm Graz"], ["id" => 357]);
        $this->update("team", ["eng_name" => "Admira"], ["id" => 358]);
        $this->update("team", ["eng_name" => "Rapid Wien"], ["id" => 359]);
        $this->update("team", ["eng_name" => "Ried"], ["id" => 360]);
        $this->update("team", ["eng_name" => "Grodig"], ["id" => 361]);
        $this->update("team", ["eng_name" => "Rheindorf Altach"], ["id" => 362]);
        $this->update("team", ["eng_name" => "Wolfsberger AC"], ["id" => 363]);
        $this->update("team", ["eng_name" => "Austria Wien"], ["id" => 364]);

        //Азербайджан премьер-лига
        $this->update("team", ["eng_name" => "Zira"], ["id" => 365]);
        $this->update("team", ["eng_name" => "AZAL Baku"], ["id" => 366]);
        $this->update("team", ["eng_name" => "Neftchi Baku"], ["id" => 367]);
        $this->update("team", ["eng_name" => "Kapaz"], ["id" => 368]);
        $this->update("team", ["eng_name" => "Khazar Lankaran"], ["id" => 369]);
        $this->update("team", ["eng_name" => "Inter Baku"], ["id" => 370]);
        $this->update("team", ["eng_name" => "Gabala"], ["id" => 371]);
        $this->update("team", ["eng_name" => "Qarabag"], ["id" => 372]);
        $this->update("team", ["eng_name" => "Ravan Baku"], ["id" => 373]);
        $this->update("team", ["eng_name" => "Sumqayit"], ["id" => 374]);

        //Болгария группа А
        $this->update("team", ["eng_name" => "Slavia Sofia"], ["id" => 375]);
        $this->update("team", ["eng_name" => "Lokomotiv Plovdiv"], ["id" => 376]);
        $this->update("team", ["eng_name" => "Botev Plovdiv"], ["id" => 377]);
        $this->update("team", ["eng_name" => "Levski Sofia"], ["id" => 378]);
        $this->update("team", ["eng_name" => "Litex Lovech"], ["id" => 379]);
        $this->update("team", ["eng_name" => "Ludogorets Razgrad"], ["id" => 380]);
        $this->update("team", ["eng_name" => "Pirin Blagoevgrad"], ["id" => 381]);
        $this->update("team", ["eng_name" => "Cherno More"], ["id" => 382]);
        $this->update("team", ["eng_name" => "Beroe"], ["id" => 383]);
        $this->update("team", ["eng_name" => "Montana"], ["id" => 384]);

        //Греция суперлига
        $this->update("team", ["eng_name" => "Panthrakikos"], ["id" => 385]);
        $this->update("team", ["eng_name" => "Asteras Tripolis"], ["id" => 386]);
        $this->update("team", ["eng_name" => "Kallonis"], ["id" => 387]);
        $this->update("team", ["eng_name" => "Iraklis 1908 Thessaloniki"], ["id" => 388]);
        $this->update("team", ["eng_name" => "AEK Athens"], ["id" => 389]);
        $this->update("team", ["eng_name" => "Platanias"], ["id" => 390]);
        $this->update("team", ["eng_name" => "Veria"], ["id" => 391]);
        $this->update("team", ["eng_name" => "PAS Giannina"], ["id" => 392]);
        $this->update("team", ["eng_name" => "PAOK"], ["id" => 393]);
        $this->update("team", ["eng_name" => "Xanthi"], ["id" => 394]);
        $this->update("team", ["eng_name" => "Atromitos Athens"], ["id" => 395]);
        $this->update("team", ["eng_name" => "Levadiakos"], ["id" => 396]);
        $this->update("team", ["eng_name" => "Olympiakos Pireas"], ["id" => 397]);
        $this->update("team", ["eng_name" => "Panionios"], ["id" => 398]);
        $this->update("team", ["eng_name" => "Panetolikos"], ["id" => 399]);
        $this->update("team", ["eng_name" => "Panathinaikos Athens"], ["id" => 400]);

        //Дания суперлига
        $this->update("team", ["eng_name" => "Nordsjaelland"], ["id" => 401]);
        $this->update("team", ["eng_name" => "SonderjyskE"], ["id" => 402]);
        $this->update("team", ["eng_name" => "Midtjylland"], ["id" => 403]);
        $this->update("team", ["eng_name" => "Viborg"], ["id" => 404]);
        $this->update("team", ["eng_name" => "Odense"], ["id" => 405]);
        $this->update("team", ["eng_name" => "Hobro"], ["id" => 406]);
        $this->update("team", ["eng_name" => "AGF Aarhus"], ["id" => 407]);
        $this->update("team", ["eng_name" => "Brondby"], ["id" => 408]);
        $this->update("team", ["eng_name" => "Aalborg"], ["id" => 409]);
        $this->update("team", ["eng_name" => "Esbjerg"], ["id" => 410]);
        $this->update("team", ["eng_name" => "Randers"], ["id" => 411]);
        $this->update("team", ["eng_name" => "FC Kobenhavn"], ["id" => 412]);

        //Польша экстракласса
        $this->update("team", ["eng_name" => "Wisla Krakow"], ["id" => 413]);
        $this->update("team", ["eng_name" => "Gornik Zabrze"], ["id" => 414]);
        $this->update("team", ["eng_name" => "Lechia Gdansk"], ["id" => 415]);
        $this->update("team", ["eng_name" => "Cracovia"], ["id" => 416]);
        $this->update("team", ["eng_name" => "Ruch Chorzow"], ["id" => 417]);
        $this->update("team", ["eng_name" => "Gornik Leczna"], ["id" => 418]);
        $this->update("team", ["eng_name" => "Zaglebie Lubin"], ["id" => 419]);
        $this->update("team", ["eng_name" => "Podbeskidzie Bielsko-Biala"], ["id" => 420]);
        $this->update("team", ["eng_name" => "Lech Poznan"], ["id" => 421]);
        $this->update("team", ["eng_name" => "Pogon Szczecin"], ["id" => 422]);
        $this->update("team", ["eng_name" => "Korona Kielce"], ["id" => 423]);
        $this->update("team", ["eng_name" => "Jagiellonia Bialystok"], ["id" => 424]);
        $this->update("team", ["eng_name" => "Slask Wroclaw"], ["id" => 425]);
        $this->update("team", ["eng_name" => "Legia Warszawa"], ["id" => 426]);
        $this->update("team", ["eng_name" => "Piast Gliwice"], ["id" => 427]);
        $this->update("team", ["eng_name" => "Nieciecza"], ["id" => 428]);

        //Хорватия первая лига
        $this->update("team", ["eng_name" => "Inter Zapresic"], ["id" => 429]);
        $this->update("team", ["eng_name" => "Rijeka"], ["id" => 430]);
        $this->update("team", ["eng_name" => "Zagreb"], ["id" => 431]);
        $this->update("team", ["eng_name" => "Osijek"], ["id" => 432]);
        $this->update("team", ["eng_name" => "Istra 1961"], ["id" => 433]);
        $this->update("team", ["eng_name" => "Slaven Belupo"], ["id" => 434]);
        $this->update("team", ["eng_name" => "Dinamo Zagreb"], ["id" => 435]);
        $this->update("team", ["eng_name" => "Hajduk Split"], ["id" => 436]);
        $this->update("team", ["eng_name" => "Split"], ["id" => 437]);
        $this->update("team", ["eng_name" => "Lokomotiva"], ["id" => 438]);

        //Чехия синот лига
        $this->update("team", ["eng_name" => "Viktoria Plzen"], ["id" => 439]);
        $this->update("team", ["eng_name" => "Slavia Prague"], ["id" => 440]);
        $this->update("team", ["eng_name" => "Vysocina Jihlava"], ["id" => 441]);
        $this->update("team", ["eng_name" => "Sparta Prague"], ["id" => 442]);
        $this->update("team", ["eng_name" => "Zbrojovka Brno"], ["id" => 443]);
        $this->update("team", ["eng_name" => "Banik Ostrava"], ["id" => 444]);
        $this->update("team", ["eng_name" => "Pribram"], ["id" => 445]);
        $this->update("team", ["eng_name" => "Jablonec"], ["id" => 446]);
        $this->update("team", ["eng_name" => "Slovacko"], ["id" => 447]);
        $this->update("team", ["eng_name" => "Dukla Prague"], ["id" => 448]);
        $this->update("team", ["eng_name" => "Teplice"], ["id" => 449]);
        $this->update("team", ["eng_name" => "Sigma Olomouc"], ["id" => 450]);
        $this->update("team", ["eng_name" => "Bohemians 1905"], ["id" => 451]);
        $this->update("team", ["eng_name" => "Zlin"], ["id" => 452]);
        $this->update("team", ["eng_name" => "Slovan Liberec"], ["id" => 453]);
        $this->update("team", ["eng_name" => "Mlada Boleslav"], ["id" => 454]);

        //Шотландия премьер-лига
        $this->update("team", ["eng_name" => "Celtic"], ["id" => 455]);
        $this->update("team", ["eng_name" => "Ross County"], ["id" => 456]);
        $this->update("team", ["eng_name" => "Hamilton"], ["id" => 457]);
        $this->update("team", ["eng_name" => "Partick Thistle"], ["id" => 458]);
        $this->update("team", ["eng_name" => "Inverness Caledonian Thistle"], ["id" => 459]);
        $this->update("team", ["eng_name" => "Motherwell"], ["id" => 460]);
        $this->update("team", ["eng_name" => "Kilmarnock"], ["id" => 461]);
        $this->update("team", ["eng_name" => "Dundee"], ["id" => 462]);
        $this->update("team", ["eng_name" => "Hearts"], ["id" => 463]);
        $this->update("team", ["eng_name" => "St Johnstone"], ["id" => 464]);
        $this->update("team", ["eng_name" => "Dundee United"], ["id" => 465]);
        $this->update("team", ["eng_name" => "Aberdeen"], ["id" => 466]);

    }

    public function safeDown()
    {

    }
}
