<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coef".
 *
 * @property integer $id
 * @property integer $match_id
 * @property integer $coef_param_id
 * @property double $value
 *
 * @property CoefParam $coefParam
 * @property Match $match
 */
class Coef extends \yii\db\ActiveRecord
{

    const TOTAL_OVER_15 = 1;
    const TOTAL_OVER_2 = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coef';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['match_id', 'coef_param_id'], 'integer'],
            [['value'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'match_id' => Yii::t('app', 'Match ID'),
            'coef_param_id' => Yii::t('app', 'Coef Param ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoefParam()
    {
        return $this->hasOne(CoefParam::className(), ['id' => 'coef_param_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatch()
    {
        return $this->hasOne(Match::className(), ['id' => 'match_id']);
    }
}
