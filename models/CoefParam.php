<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coef_param".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Coef[] $coefs
 */
class CoefParam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coef_param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoefs()
    {
        return $this->hasMany(Coef::className(), ['coef_param_id' => 'id']);
    }
}
