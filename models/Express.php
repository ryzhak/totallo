<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "express".
 *
 * @property integer $id
 * @property integer $unix_date
 * @property integer $league_id
 *
 * @property ExpressPart[] $expressParts
 */
class Express extends \yii\db\ActiveRecord
{

    const WORLD_LEAGUE_ID = 0;
    const MIN_COEF = 1.7;
    const STREAK_COUNT = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'express';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unix_date', 'league_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'unix_date' => Yii::t('app', 'Unix Date'),
            'league_id' => Yii::t('app', 'League ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpressParts()
    {
        return $this->hasMany(ExpressPart::className(), ['express_id' => 'id']);
    }

    /**
     * Возвращает массив игр для экспресса
     *
     * @param array $matches массив игр
     * @return array
     */
    public static function SortByGoalsProb($matches){

        usort($matches, function($m1, $m2){

            $m1PredictGoals = $m1['predict_goals_home'] + $m1['predict_goals_away'];
            $m2PredictGoals = $m2['predict_goals_home'] + $m2['predict_goals_away'];

            //если кол-во голов одинаковое
            if($m1PredictGoals == $m2PredictGoals){

                //смотрим по серии забитых мячей в 6 матчах
                $m1StreakSum = Team::getStreak($m1['home_team_id'], self::STREAK_COUNT) + Team::getStreak($m1['away_team_id'], self::STREAK_COUNT);
                $m2StreakSum = Team::getStreak($m2['home_team_id'], self::STREAK_COUNT) + Team::getStreak($m2['away_team_id'], self::STREAK_COUNT);

                //если одинаковые стрики
                if($m1StreakSum == $m2StreakSum){

                    //смотрим по сумме забитых мячей в играх команд за последние матчи
                    $m1AllGoalsSum = Team::getScoredGoals($m1['home_team_id'], self::STREAK_COUNT) + Team::getScoredGoals($m1['away_team_id'], self::STREAK_COUNT);
                    $m2AllGoalsSum = Team::getScoredGoals($m2['home_team_id'], self::STREAK_COUNT) + Team::getScoredGoals($m2['away_team_id'], self::STREAK_COUNT);

                    if($m1AllGoalsSum == $m2AllGoalsSum){
                        return 0;
                    }

                    return ($m1AllGoalsSum < $m2AllGoalsSum) ? 1 : -1 ;

                }

                return ($m1StreakSum < $m2StreakSum) ? 1 : -1 ;

            }

            return ($m1PredictGoals < $m2PredictGoals) ? 1 : -1 ;

        });

        return $matches;

    }

    /**
     * Возвращает матчи для экспресса
     *
     * @param $matches
     * @return array
     */
    public static function getMatchesByCoef($matches){

        $res = [];

        if(count($matches) == 0){
            return $res;
        }

        if(count($matches) == 1){

            //если находим ТБ 1.5 для игры
            if($mTO15 = Coef::findOne(['match_id' => $matches[0]['id'], 'coef_param_id' => Coef::TOTAL_OVER_15])){
                if(!empty($mTO15->value)){
                    //если ТБ1.5 > 1.7 то возвращаем эту игру
                    if($mTO15->value >= self::MIN_COEF){
                        $matches[0]['coef_param_id'] = Coef::TOTAL_OVER_15;
                        return [$matches[0]];
                    }
                }
            }

            //если находим ТБ 2 для игры
            if($mTO2 = Coef::findOne(['match_id' => $matches[0]['id'], 'coef_param_id' => Coef::TOTAL_OVER_2])){
                if(!empty($mTO2->value)){
                    if($mTO2->value >= self::MIN_COEF){
                        $matches[0]['coef_param_id'] = Coef::TOTAL_OVER_2;
                        return [$matches[0]];
                    }
                }
            }

            return $res;

        }

        //иначе команд >= 2
        $prevMaxMatch = $matches[0];

        //если предыдущий находим ТБ 1.5 для игры
        if($mPrevTO15 = Coef::findOne(['match_id' => $prevMaxMatch['id'], 'coef_param_id' => Coef::TOTAL_OVER_15])){
            if(!empty($mPrevTO15->value)){
                //если ТБ1.5 > 1.7 то возвращаем эту игру
                if($mPrevTO15->value >= self::MIN_COEF) return [$prevMaxMatch];
            }
        }

        //если находим ТБ 2 для игры
        if($mPrevTO2 = Coef::findOne(['match_id' => $prevMaxMatch['id'], 'coef_param_id' => Coef::TOTAL_OVER_2])){
            if(!empty($mPrevTO2->value)){
                $prevMaxMatch['coef_param_id'] = Coef::TOTAL_OVER_2;
                if($mPrevTO2->value >= self::MIN_COEF) return [$prevMaxMatch];
            }
        }

        for($i = 1; $i < count($matches); $i++){

            //если находим ТБ 1.5 для игры
            if($mTO15 = Coef::findOne(['match_id' => $matches[$i]['id'], 'coef_param_id' => Coef::TOTAL_OVER_15])){
                if(!empty($mTO15->value)){

                    $matches[$i]['coef_param_id'] = Coef::TOTAL_OVER_15;

                    if($mTO15->value * $mPrevTO15->value >= self::MIN_COEF){
                        $prevMaxMatch['coef_param_id'] = Coef::TOTAL_OVER_15;
                        return [$prevMaxMatch, $matches[$i]];
                    } else {
                        if($mTO15->value * $mPrevTO2->value >= self::MIN_COEF){
                            $prevMaxMatch['coef_param_id'] = Coef::TOTAL_OVER_2;
                            return [$prevMaxMatch, $matches[$i]];
                        }

                    }
                }
            }

            //если находим ТБ 2 для игры
            if($mTO2 = Coef::findOne(['match_id' => $matches[$i]['id'], 'coef_param_id' => Coef::TOTAL_OVER_2])){
                if(!empty($mTO2->value)){

                    $matches[$i]['coef_param_id'] = Coef::TOTAL_OVER_2;

                    if($mTO2->value * $mPrevTO2->value >= self::MIN_COEF){
                        $prevMaxMatch['coef_param_id'] = Coef::TOTAL_OVER_2;
                        return [$prevMaxMatch, $matches[$i]];
                    }
                }
            }

            //если коэффициенты этого матча больше предыдущего максимального, то обновляем
            if($mTO15 > $mPrevTO15 || $mTO2 > $mPrevTO2){
                $prevMaxMatch = $matches[$i];
                $mPrevTO15 = Coef::findOne(['match_id' => $matches[$i]['id'], 'coef_param_id' => Coef::TOTAL_OVER_15]);
                $mPrevTO2 = Coef::findOne(['match_id' => $matches[$i]['id'], 'coef_param_id' => Coef::TOTAL_OVER_2]);
            }

        }

        return $res;

    }

    /**
     * Возвращает общий коэффициент для экспресса
     * @param $expressID
     * @return float
     */
    public static function getCoefSum($expressID){
        $mExpress = self::findOne($expressID);
        $coefSum = 1;
        foreach($mExpress->expressParts as $mExpressPart){
            $coefSum *= Coef::findOne(['match_id' => $mExpressPart->match_id, 'coef_param_id' => $mExpressPart->coef_param_id])->value;
        }
        return $coefSum;
    }

    /**
     * Возвращает вин рейт экспрессов в процентах
     * @param $leagueID
     * @return mixed
     */
    public static function getWinRate($leagueID){

        $mExpresses = self::findAll(['league_id' => $leagueID]);
        $countAll = 0;
        $countNotYetPlayed = 0;
        $loseCount = 0;

        //перебираем все экспрессы
        foreach($mExpresses as $mExpress){
            if(!empty($mExpress->expressParts)) $countAll++;
            //перебираем все матчи в экспрессах
            foreach($mExpress->expressParts as $mExpressPart){
                //находим матч
                $mMatch = Match::findOne($mExpressPart->match_id);
                //если он еще не состоялся, то дальше
                if($mMatch->home_team_goals == "-" || $mMatch->away_team_goals == "-"){
                    $countNotYetPlayed++;
                    break;
                } else {
                    $sumRealGoals = $mMatch->home_team_goals + $mMatch->away_team_goals;
                    //если было ТБ1.5 на эту игру
                    if($mExpressPart->coef_param_id == Coef::TOTAL_OVER_15){
                        if($sumRealGoals < 1.5){
                            $loseCount++;
                            break;
                        }
                    }
                    //если было ТБ2 на эту игру
                    if($mExpressPart->coef_param_id == Coef::TOTAL_OVER_2){
                        if($sumRealGoals < 2){
                            $loseCount++;
                            break;
                        }
                    }
                }
            }
        }
        if($countNotYetPlayed == $countAll) return 0;
        if($loseCount == 0) return 100;
        if($loseCount == $countAll) return 0;
        return 100 - round(($loseCount / $countAll) * 100, 2);

    }

    /**
     * Возвращает средний коэффициент для определенной лиги
     *
     * @param $leagueID
     * @return float
     */
    public static function getAvgPrice($leagueID){

        $coefAvgSum = 0;
        $count = 0;

        $mExpresses = Express::findAll(['league_id' => $leagueID]);

        foreach($mExpresses as $mExpress){

            if(empty($mExpress->expressParts)) continue;

            $count++;
            $price = 1;
            foreach($mExpress->expressParts as $mExpressPart){
                $price *= Coef::findOne(['match_id' => $mExpressPart->match_id, 'coef_param_id' => $mExpressPart->coef_param_id])->value;
            }

            $coefAvgSum += $price;

        }

        if($count == 0) return 0;
        return round($coefAvgSum / $count, 2);

    }

    /**
     * Возвращает последние $count экспрессов для всех лиг
     *
     * @param $count
     * @return mixed
     */
    public static function getLast($count){

        $res = [];

        //для World. All matches.
        $worldLeague = [
            'id' => 0,
            'country_name' => 'World',
            'league_name' => 'All matches'
        ];

        $resForLeague = [];

        $resForLeague['league_id'] = $worldLeague['id'];
        $resForLeague['country_name'] = $worldLeague['country_name'];
        $resForLeague['league_name'] = $worldLeague['league_name'];
        $resForLeague['win_rate'] = self::getWinRate($worldLeague['id']);
        $resForLeague['avg_price'] = self::getAvgPrice($worldLeague['id']);

        //находим последние экспрессы
        $mExpresses = self::find()->where("league_id = :league_id", [
            ":league_id" => $worldLeague['id']
        ])->orderBy('unix_date DESC')->limit($count)->all();

        //перебираем все экспрессы
        $resForExpresses = [];
        $i = 0;
        foreach($mExpresses as $mExpress){
            $resForExpresses[$i]['unix_date'] = $mExpress->unix_date;

            //перебираем все игры в экспрессах
            $j = 0;
            $matches = [];
            foreach($mExpress->expressParts as $mExpressPart){
                $matches[$j]['date'] = $mExpressPart->match->unix_date;
                $matches[$j]['country'] = $mExpressPart->match->league->country->eng_name;
                $matches[$j]['league'] = $mExpressPart->match->league->eng_name;
                $matches[$j]['home_team_name'] = $mExpressPart->match->homeTeam->eng_name;
                $matches[$j]['away_team_name'] = $mExpressPart->match->awayTeam->eng_name;
                $matches[$j]['bet_type'] = $mExpressPart->coef_param_id == Coef::TOTAL_OVER_15 ? "Total Goals Over 1.5" : "Total Goals Over 2";
                $matches[$j]['price'] = Coef::findOne([
                    'match_id' => $mExpressPart->match_id,
                    'coef_param_id' => $mExpressPart->coef_param_id
                ])->value;
                $j++;
            }

            $resForExpresses[$i]['matches'] = $matches;
            $i++;
        }

        $resForLeague['expresses'] = $resForExpresses;

        $res[] = $resForLeague;


        //для всех остальных лиг по отдельности
        foreach(League::find()->all() as $mLeague){

            $resForLeague = [];

            $resForLeague['league_id'] = $mLeague->id;
            $resForLeague['country_name'] = $mLeague->country->eng_name;
            $resForLeague['league_name'] = $mLeague->eng_name;
            $resForLeague['win_rate'] = self::getWinRate($mLeague->id);
            $resForLeague['avg_price'] = self::getAvgPrice($mLeague->id);

            //находим последние экспрессы
            $mExpresses = self::find()->where("league_id = :league_id", [
                ":league_id" => $mLeague->id
            ])->orderBy('unix_date DESC')->limit($count)->all();

            //перебираем все экспрессы
            $resForExpresses = [];
            $i = 0;
            foreach($mExpresses as $mExpress){
                $resForExpresses[$i]['unix_date'] = $mExpress->unix_date;

                //перебираем все игры в экспрессах
                $j = 0;
                $matches = [];
                foreach($mExpress->expressParts as $mExpressPart){
                    $matches[$j]['date'] = $mExpressPart->match->unix_date;
                    $matches[$j]['country'] = $mExpressPart->match->league->country->eng_name;
                    $matches[$j]['league'] = $mExpressPart->match->league->eng_name;
                    $matches[$j]['home_team_name'] = $mExpressPart->match->homeTeam->eng_name;
                    $matches[$j]['away_team_name'] = $mExpressPart->match->awayTeam->eng_name;
                    $matches[$j]['bet_type'] = $mExpressPart->coef_param_id == Coef::TOTAL_OVER_15 ? "Total Goals Over 1.5" : "Total Goals Over 2";
                    $matches[$j]['price'] = Coef::findOne([
                        'match_id' => $mExpressPart->match_id,
                        'coef_param_id' => $mExpressPart->coef_param_id
                    ])->value;
                    $j++;
                }

                $resForExpresses[$i]['matches'] = $matches;
                $i++;
            }

            $resForLeague['expresses'] = $resForExpresses;

            $res[] = $resForLeague;

        }

        return $res;

    }

}
