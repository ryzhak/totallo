<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "express_part".
 *
 * @property integer $id
 * @property integer $match_id
 * @property integer $express_id
 * @property integer $predict_goals_home
 * @property integer $predict_goals_away
 *
 * @property Express $express
 * @property Match $match
 */
class ExpressPart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'express_part';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['match_id', 'express_id', 'predict_goals_home', 'predict_goals_away', 'coef_param_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'match_id' => Yii::t('app', 'Match ID'),
            'express_id' => Yii::t('app', 'Express ID'),
            'predict_goals_home' => Yii::t('app', 'Predict Goals Home'),
            'predict_goals_away' => Yii::t('app', 'Predict Goals Away'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpress()
    {
        return $this->hasOne(Express::className(), ['id' => 'express_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatch()
    {
        return $this->hasOne(Match::className(), ['id' => 'match_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoefParam()
    {
        return $this->hasOne(CoefParam::className(), ['id' => 'coef_param_id']);
    }
}
