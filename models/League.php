<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "league".
 *
 * @property integer $id
 * @property string $name
 * @property string $eng_name
 * @property integer $country_id
 * @property integer $season_id
 * @property string $championat_table_url
 * @property string $marathon_url
 *
 * @property Country $country
 * @property Season $season
 * @property Match[] $matches
 * @property Team[] $teams
 */
class League extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'league';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'season_id'], 'integer'],
            [['name', 'eng_name', 'championat_table_url', 'marathon_url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'eng_name' => Yii::t('app', 'Eng Name'),
            'country_id' => Yii::t('app', 'Country ID'),
            'season_id' => Yii::t('app', 'Season ID'),
            'championat_table_url' => Yii::t('app', 'Championat Table Url'),
            'marathon_url' => Yii::t('app', 'Marathon Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Season::className(), ['id' => 'season_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatches()
    {
        return $this->hasMany(Match::className(), ['league_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(Team::className(), ['league_id' => 'id']);
    }

    /**
     * Возвращает html код главного меню
     */
    public static function getMenu(){
        $menu = "";
        foreach(self::find()->all() as $mLeague){
            $menu .= "<li><a href='/site/accumulators?league_id={$mLeague->id}'>{$mLeague->country->eng_name}. {$mLeague->eng_name}.</a></li>";
        }
        return $menu;
    }

}
