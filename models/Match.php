<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "match".
 *
 * @property integer $id
 * @property integer $home_team_id
 * @property integer $away_team_id
 * @property integer $league_id
 * @property integer $tour_number
 * @property string $home_team_goals
 * @property string $away_team_goals
 * @property integer $unix_date
 * @property string $predict_goals_home
 * @property string $predict_goals_away
 *
 * @property Coef[] $coefs
 * @property League $league
 * @property Team $homeTeam
 * @property Team $awayTeam
 */
class Match extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'match';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['home_team_id', 'away_team_id', 'league_id', 'tour_number', 'unix_date'], 'integer'],
            [['home_team_goals', 'away_team_goals', 'predict_goals_home', 'predict_goals_away'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'home_team_id' => Yii::t('app', 'Home Team ID'),
            'away_team_id' => Yii::t('app', 'Away Team ID'),
            'league_id' => Yii::t('app', 'League ID'),
            'tour_number' => Yii::t('app', 'Tour Number'),
            'home_team_goals' => Yii::t('app', 'Home Team Goals'),
            'away_team_goals' => Yii::t('app', 'Away Team Goals'),
            'unix_date' => Yii::t('app', 'Unix Date'),
            'predict_goals_home' => Yii::t('app', 'Predict Goals Home'),
            'predict_goals_away' => Yii::t('app', 'Predict Goals Away'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoefs()
    {
        return $this->hasMany(Coef::className(), ['match_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeague()
    {
        return $this->hasOne(League::className(), ['id' => 'league_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHomeTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'home_team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAwayTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'away_team_id']);
    }
}
