<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stat".
 *
 * @property integer $id
 * @property integer $team_id
 * @property integer $stat_param_id
 * @property string $all_sum
 * @property string $all_avg
 * @property string $home_sum
 * @property string $home_avg
 * @property string $away_sum
 * @property string $away_avg
 */
class Stat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'stat_param_id'], 'integer'],
            [['all_sum', 'all_avg', 'home_sum', 'home_avg', 'away_sum', 'away_avg'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'team_id' => Yii::t('app', 'Team ID'),
            'stat_param_id' => Yii::t('app', 'Stat Param ID'),
            'all_sum' => Yii::t('app', 'All Sum'),
            'all_avg' => Yii::t('app', 'All Avg'),
            'home_sum' => Yii::t('app', 'Home Sum'),
            'home_avg' => Yii::t('app', 'Home Avg'),
            'away_sum' => Yii::t('app', 'Away Sum'),
            'away_avg' => Yii::t('app', 'Away Avg'),
        ];
    }
}
