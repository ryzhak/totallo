<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property integer $id
 * @property string $name
 * @property string $eng_name
 * @property integer $league_id
 *
 * @property Match[] $matches
 * @property Match[] $matches0
 * @property League $league
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['league_id'], 'integer'],
            [['name', 'eng_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'eng_name' => Yii::t('app', 'Eng Name'),
            'league_id' => Yii::t('app', 'League ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatches()
    {
        return $this->hasMany(Match::className(), ['home_team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatches0()
    {
        return $this->hasMany(Match::className(), ['away_team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeague()
    {
        return $this->hasOne(League::className(), ['id' => 'league_id']);
    }

    /**
     * Возврашает кол-во матчей, в которых команда $teamID забивала хотя бы 1 мяч из $count последних
     *
     * @param $teamID
     * @param $count
     * @return integer
     */
    public static function getStreak($teamID, $count){

        $streak = 0;

        //находим последние $count матчей команды
        $mMatches = Match::find()->where("(home_team_id = :home_team_id OR away_team_id = :away_team_id)
            AND (home_team_goals != '-' OR away_team_goals != '-')",[
            ':home_team_id' => $teamID,
            ':away_team_id' => $teamID
        ])
            ->orderBy('unix_date DESC')
            ->limit($count)
            ->all();

        foreach($mMatches as $mMatch){
            if($mMatch->home_team_id == $teamID && $mMatch->home_team_goals > 0) $streak ++;
            if($mMatch->away_team_id == $teamID && $mMatch->away_team_goals > 0) $streak ++;
        }

        return $streak;

    }

    /**
     * Возвращает общее кол-во забитых и пропущенных мячей командой $teamID в последних $countLastGames матчах
     *
     * @param $teamID
     * @param $countLastGames
     * @return integer
     */
    public static function getScoredGoals($teamID, $countLastGames){
        $scoredGoals = 0;
        //находим последние $count матчей команды
        $mMatches = Match::find()->where("(home_team_id = :home_team_id OR away_team_id = :away_team_id)
            AND (home_team_goals != '-' OR away_team_goals != '-')",[
            ':home_team_id' => $teamID,
            ':away_team_id' => $teamID
        ])
            ->orderBy('unix_date DESC')
            ->limit($countLastGames)
            ->all();
        foreach($mMatches as $mMatch){
            $scoredGoals += $mMatch->home_team_goals + $mMatch->away_team_goals;
        }
        return $scoredGoals;
    }

}
