<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\models\League;
use yii\helpers\Html;
use app\assets\MaterializeAsset;

MaterializeAsset::register($this);
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>

    <!-- google analytics code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-55203985-4', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body>
<?php $this->beginBody() ?>

    <div class="container">

        <!-- Dropdown Structure -->
        <ul id="dropdown1" class="dropdown-content">
            <li><a href="/site/accumulators">World. All matches.</a></li>
            <?= League::getMenu() ?>
        </ul>
        <!-- панель навигации -->
        <nav>
            <div class="nav-wrapper teal darken-4">
                <a href="/" class="brand-logo">Totallo <sup>beta</sup></a>
                <!-- основное меню -->
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1">World Football Leagues<i class="material-icons right">arrow_drop_down</i></a></li>
                </ul>
                <!-- боковое меню в мобильной версии -->
                <ul id="slide-out" class="side-nav">
                    <li><a href="/site/accumulators">World. All matches.</a></li>
                    <?= League::getMenu() ?>
                </ul>
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            </div>
        </nav>

        <main>
            <?= $content ?>
        </main>


        <footer class="page-footer teal darken-4">
            <div class="footer-copyright">
                <div class="row">
                    <div class="s12 center-align">
                        © Totallo.net 2015
                    </div>
                </div>
            </div>
        </footer>

    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
