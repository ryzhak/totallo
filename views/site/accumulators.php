<?// var_dump($mExpresses); ?>
<?
    use app\models\Coef;
    use app\models\Express;
    use app\models\League;
?>

<div class="card teal darken-3 z-depth-2">
    <div class="card-content center-align">
        <div class="card-title">
            <?= ($leagueID == 0) ? "World" : League::findOne($leagueID)->country->eng_name?> -
            <?= ($leagueID == 0) ? "All matches" : League::findOne($leagueID)->eng_name?>
        </div>
        <span class="card-title">
            Win rate: <?= Express::getWinRate($leagueID) ?>%
            Average price: <?= Express::getAvgPrice($leagueID) ?>
        </span>
    </div>
</div>

<?
foreach($mExpresses as $mExpress): ?>
    <?if(empty($mExpress->expressParts)):?>
        <div class="card teal darken-3 z-depth-2">
            <div class="card-content">
                <span class="card-title"><?=date("d.m.Y",$mExpress->unix_date)?>  - No bets</span>
            </div>
        </div>
    <?else:?>

        <div class="card teal darken-3 z-depth-2">
        <div class="card-content">
        <span class="card-title"><?=date("d.m.Y",$mExpress->unix_date)?></span>
        <table class="responsive-table">
            <thead>
                <tr>
                    <th data-field="id">Date</th>
                    <th data-field="name">League</th>
                    <th data-field="name">Event</th>
                    <th data-field="price">Bet Type</th>
                    <th data-field="price">Price</th>
                </tr>
            </thead>

            <tbody>
                <?foreach($mExpress->expressParts as $mExpressPart):?>
                    <tr>
                        <td><?=date("d.m.Y H:i",$mExpressPart->match->unix_date)?> (GMT+3)</td>
                        <td><?=$mExpressPart->match->league->country->eng_name?>. <?=$mExpressPart->match->league->eng_name?>.</td>
                        <td><?=$mExpressPart->match->homeTeam->eng_name?> - <?=$mExpressPart->match->awayTeam->eng_name?></td>
                        <td>
                            <? if ($mExpressPart->coef_param_id == 1) echo "Total Goals Over 1.5"; ?>
                            <? if ($mExpressPart->coef_param_id == 2) echo "Total Goals Over 2"; ?>
                        </td>
                        <td><? echo Coef::findOne(['match_id' => $mExpressPart->match_id, 'coef_param_id' => $mExpressPart->coef_param_id])->value; ?></td>
                    </tr>
                <?endforeach;?>
            </tbody>
        </table>
        </div>
            <div class="card-action right-align">
                <span>Total price: <?= round(Express::getCoefSum($mExpress->id), 2);?></span>
            </div>
        </div>
    <?endif;?>
<?endforeach;?>