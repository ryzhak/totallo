<?
use app\models\League;
use app\models\StatParam;

?>

<div class="card teal darken-3 z-depth-2">
    <div class="card-content center-align">
        <span class="card-title"><strong>How it works</strong></span>
    </div>
</div>

<div class="card teal darken-3 z-depth-2">
    <div class="card-content center-align">
        <span class="card-title">Step 1. Analyze 27 football leagues.</span>
        <?foreach(League::find()->all() as $mLeague):?>
            <p><?=$mLeague->country->eng_name?>. <?=$mLeague->eng_name?>.</p>
        <?endforeach;?>
    </div>
</div>

<div class="card teal darken-3 z-depth-2">
    <div class="card-content center-align">
        <span class="card-title">Step 2. Analyze team statistics.</span>
        <p>Matches played</p>
        <p>Wins</p>
        <p>Draws</p>
        <p>Loses</p>
        <p>Points</p>
        <p>Scored goals</p>
        <p>Missed goals</p>
        <p>Goal difference</p>
        <p>Yellow cards</p>
        <p>Second yellow cards</p>
        <p>Red cards</p>
        <p>Penalties</p>
        <p>Spectators</p>
        <p>Goal chances</p>
        <p>Shots</p>
        <p>Shots on goal</p>
        <p>Shot accuracy</p>
        <p>Shot realization</p>
        <p>Bars</p>
        <p>Corners</p>
        <p>Offsides</p>
        <p>Fouls</p>
        <p>Ball possession</p>
        <p>Average footballer rate</p>
    </div>
</div>

<div class="card teal darken-3 z-depth-2">
    <div class="card-content center-align">
        <span class="card-title">Step 3. Create neural network to harness all this data.</span>
    </div>
</div>

<div class="card teal darken-3 z-depth-2">
    <div class="card-content center-align">
        <span class="card-title">Step 4. Get accumulator bets with the highest probability of goals count scored in a match.</span>
    </div>
</div>